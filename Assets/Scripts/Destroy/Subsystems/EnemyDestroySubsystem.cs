﻿using Animation.Destroy.Components;
using Common.Components;
using Destroy.Components;
using Leopotam.Ecs;
using Spawn.Components;
using Units.Battle.Enemy.Components;

namespace Destroy.Subsystems
{
    public class EnemyDestroySubsystem : IEcsRunSystem
    {
        private EcsFilter<EnemyTag, UnitComponent, DestroyEvent> _filter;

        private EcsWorld _world;

        public void Run()
        {
            foreach (var i in _filter)
            {
                _filter.GetEntity(i).Get<DestroyAnimationEvent>().Callback = () =>
                {
                    _world.NewEntity().Get<SpawnLampEvent>().SpawnPosition = _filter.Get2(i).Position;
                };
            }
        }
    }
}