﻿using Animation.Destroy.Components;
using Audio;
using Core;
using Destroy.Components;
using Leopotam.Ecs;
using Popups;
using Popups.ChangeEquipmentPopup.Providers;
using Units.Collect.Workshop.Components;

namespace Destroy.Subsystems
{
    public class WorkshopDestroySubsystem : IEcsRunSystem
    {
        private EcsFilter<WorkshopComponent, DestroyEvent> _filter;

        public void Run()
        {
            foreach (var i in _filter)
            {
                PopupManager.OpenPopup<ChangeEquipmentPopup>();
                AudioManager.PlayAudio(SoundType.UIOpenBonus);
                _filter.GetEntity(i).Get<DestroyAnimationEvent>();
            }
        }
    }
}