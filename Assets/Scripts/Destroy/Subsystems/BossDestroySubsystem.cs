﻿using Animation.Destroy.Components;
using Audio;
using Core;
using Destroy.Components;
using Leopotam.Ecs;
using Leopotam.Ecs.Ui.Systems;
using Popups;
using Popups.WinPopup.Providers;
using Units.Battle.Boss.Components;

namespace Destroy.Subsystems
{
    public class BossDestroySubsystem : IEcsRunSystem
    {
        private EcsFilter<BossTag, DestroyEvent> _filter;

        private EcsUiEmitter _canvas;

        public void Run()
        {
            foreach (var i in _filter)
            {
                PopupManager.OpenPopup<WinPopup>();
                AudioManager.PlayAudio(SoundType.Win);
                
                _filter.GetEntity(i).Get<DestroyAnimationEvent>();
                _filter.GetEntity(i).Del<DestroyEvent>();
            }
        }
    }
}