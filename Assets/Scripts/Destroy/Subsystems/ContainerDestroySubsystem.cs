﻿using Animation.Destroy.Components;
using Audio;
using Common.Components;
using Core;
using Destroy.Components;
using Leopotam.Ecs;
using Popups;
using Popups.ChangeEquipmentPopup.Providers;
using Spawn.Components;
using Units.Battle.Container.Components;

namespace Destroy.Subsystems
{
    public class ContainerDestroySubsystem : IEcsRunSystem
    {
        private EcsFilter<ContainerTag, UnitComponent, DestroyEvent> _filter;

        private EcsWorld _world;
        
        public void Run()
        {
            foreach (var i in _filter)
            {
                _filter.GetEntity(i).Get<DestroyAnimationEvent>().Callback = () =>
                {
                    _world.NewEntity().Get<SpawnWorkshopEvent>().SpawnPosition = _filter.Get2(i).Position;
                };
            }
        }
    }
}