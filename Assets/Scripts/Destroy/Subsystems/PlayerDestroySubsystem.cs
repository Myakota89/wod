﻿using Animation.Destroy.Components;
using Audio;
using Core;
using Destroy.Components;
using Leopotam.Ecs;
using Leopotam.Ecs.Ui.Systems;
using Player;
using Popups;
using Popups.LosePopup.Providers;

namespace Destroy.Subsystems
{
    public class PlayerDestroySubsystem : IEcsRunSystem
    {
        private EcsFilter<PlayerTag, DestroyEvent> _filter;

        private EcsUiEmitter _canvas;

        public void Run()
        {
            foreach (var i in _filter)
            {
                PopupManager.OpenPopup<LosePopup>();
                AudioManager.PlayAudio(SoundType.Lose);
                
                _filter.GetEntity(i).Get<DestroyAnimationEvent>();
                _filter.GetEntity(i).Del<DestroyEvent>();
            }
        }
    }
}