﻿using Animation.Destroy.Components;
using Destroy.Components;
using Leopotam.Ecs;
using Player;
using UI.Events;
using Units.Collect.Lamp.Components;

namespace Destroy.Subsystems
{
    public class LampDestroySubsystem : IEcsRunSystem
    {
        private EcsFilter<LampComponent, DestroyEvent> _filter;
        private EcsFilter<PlayerTag> _playerEntity;

        private EcsWorld _world;

        public void Run()
        {
            foreach (var i in _filter)
            {
                _world.NewEntity().Get<CoinsIncreaseEvent>().Value = _filter.Get1(i).LampCount;
                _filter.GetEntity(i).Get<DestroyAnimationEvent>();
            }
        }
    }
}