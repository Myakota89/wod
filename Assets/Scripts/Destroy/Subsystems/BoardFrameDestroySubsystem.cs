﻿using Common.Components;
using Destroy.Components;
using Leopotam.Ecs;
using Player;
using UnityEngine;

namespace Destroy.Subsystems
{
    public class BoardFrameDestroySubsystem : IEcsRunSystem
    {
        private EcsFilter<BoardFrameDestroyEvent> _destroyFilter;
        private EcsFilter<UnitComponent, ViewComponent> _filter;
        private EcsFilter<PlayerTag, UnitComponent> _playerEntity;

        public void Run()
        {
            foreach (var i in _destroyFilter)
            {
                foreach (var idx in _filter)
                {
                    ref var position = ref _filter.Get1(idx).Position;
                    ref var view = ref _filter.Get2(idx);

                    if (position.x < _playerEntity.Get2(0).Position.x)
                    {
                        GameObject.DestroyImmediate(view.Transform.gameObject);
                        _filter.GetEntity(idx).Destroy();
                    }
                }
            }
        }
    }
}