﻿using Animation.Destroy.Components;
using Destroy.Components;
using Heal.Events;
using Input.Components;
using Leopotam.Ecs;
using Player;
using Units.Collect.Healer.Components;

namespace Destroy.Subsystems
{
    public class HealDestroySubsystem : IEcsRunSystem
    {
        private EcsFilter<HealComponent, DestroyEvent> _filter;
        private EcsFilter<PlayerTag> _playerEntity;
        private EcsFilter<InputComponent> _inputEntity;

        public void Run()
        {
            foreach (var i in _filter)
            {
                ref var heal = ref _filter.Get1(i);
                ref var playerHeal = ref _playerEntity.GetEntity(0).Get<HealEvent>();

                playerHeal.ArmorHeal += heal.ArmorHeal;
                playerHeal.HealthHeal += heal.HealthHeal;
                
                _filter.GetEntity(i).Get<DestroyAnimationEvent>();
            }
        }
    }
}