﻿using Common.Components;
using Destroy.Components;
using Destroy.Subsystems;
using Leopotam.Ecs;
using Leopotam.Ecs.Ui.Systems;

namespace Destroy.Systems
{
    public class DestroySystem : IEcsInitSystem, IEcsRunSystem, IEcsDestroySystem
    {
        private EcsWorld _world;
        private EcsSystems _subsystems;
        private EcsUiEmitter _uiEmitter;

        public void Init()
        {
            _subsystems = new EcsSystems(_world);
            _subsystems
                .Add(new BoardFrameDestroySubsystem())
                .Add(new EnemyDestroySubsystem())
                .Add(new BossDestroySubsystem())
                .Add(new PlayerDestroySubsystem())
                .Add(new HealDestroySubsystem())
                .Add(new ContainerDestroySubsystem())
                .Add(new LampDestroySubsystem())
                .Add(new WorkshopDestroySubsystem())
                .OneFrame<BoardFrameDestroyEvent>()
                .OneFrame<DestroyEvent>()
                .Inject(_uiEmitter)
                .Init();
        }

        public void Run()
        {
            _subsystems?.Run();
        }

        public void Destroy()
        {
            _subsystems?.Destroy();
        }
    }
}