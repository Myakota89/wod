﻿using Common.Components;
using UnityEngine;
using Voody.UniLeo;

namespace Common.Providers
{
    public class UnitProvider : MonoProvider<UnitComponent>
    {
        public void SetPosition(Vector2Int position)
        {
            value.Position = position;
        }
    }
}