﻿using System;
using UnityEngine;

namespace Common.Components
{
    [Serializable]
    public struct ViewComponent 
    {
        public Animator Animator;
        public SpriteRenderer CellSprite;
        public Transform Transform;
    }
}