﻿using System;
using UnityEngine;

namespace Common.Components
{
    [Serializable]
    public struct ColliderComponent
    {
        public Collider2D Collider;
    }
}