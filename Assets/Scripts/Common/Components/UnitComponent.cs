﻿using System;
using Enum;
using UnityEngine;

namespace Common.Components
{
    [Serializable]
    public struct UnitComponent
    {
        public Vector2Int Position;
    }
}