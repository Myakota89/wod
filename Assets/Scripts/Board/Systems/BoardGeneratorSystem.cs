﻿using Board.Components;
using Board.Events;
using Leopotam.Ecs;
using Popups;
using Spawn.Components;
using UnityEngine;

namespace Board.Systems
{
    public class BoardGeneratorSystem : IEcsRunSystem
    {
        private EcsFilter<BoardComponent, GenerateBoardEvent> boardEntity;
        private EcsWorld _world;

        public void Run()
        {
            foreach (var i in boardEntity)
            {
                ref var board = ref boardEntity.Get1(i);

                for (int x = 0; x < board.Width; x++)
                {
                    for (int y = 0; y < board.Height; y++)
                    {
                        var position = new Vector2Int(x, y);
                        var entity = _world.NewEntity();

                        if (x == 0 && y == 1)
                        {
                            ref var spawnPlayerComponent = ref entity.Get<SpawnPlayerEvent>();
                            spawnPlayerComponent.SpawnPosition = position;
                        }
                        else if (x == board.Width - 1 && y == 2)
                        {
                            ref var spawnBossComponent = ref entity.Get<SpawnBossEvent>();
                            spawnBossComponent.SpawnPosition = position;
                        }
                        else
                        {
                            SpawnRandomUnit(ref entity, position);
                        }
                    }
                }
            }
        }

        private void SpawnRandomUnit(ref EcsEntity entity, Vector2Int position)
        {
            var rnd = Random.value;
            if (rnd > 0.75f)
            {
                ref var spawnContainerComponent = ref entity.Get<SpawnContainerEvent>();
                spawnContainerComponent.SpawnPosition = position;
                spawnContainerComponent.GameObject = Resources.Load<GameObject>("Prefabs/Container/Container 1");
            }
            else if (rnd > 0.5f)
            {
                ref var spawnHealComponent = ref entity.Get<SpawnHealEvent>();
                spawnHealComponent.SpawnPosition = position;
            }
            else
            {
                ref var spawnEnemyComponent = ref entity.Get<SpawnEnemyEvent>();
                spawnEnemyComponent.SpawnPosition = position;
                spawnEnemyComponent.GameObject = Resources.Load<GameObject>("Prefabs/Enemies/Enemy1");
            }
        }
    }
}