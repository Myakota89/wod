﻿using System;
using UnityEngine;

namespace Board.Components
{
    [Serializable]
    public struct BoardComponent
    {
        public int Width;
        public int Height;
        public Transform BoardParent;
    }
}