﻿using Abilities.AbilityPanel.Common.Events;
using Audio;
using CellsMove.Events;
using CellsMove.Subsystems;
using Core;
using Leopotam.Ecs;
using UnityEngine;

namespace CellsMove.Systems
{
    public class CellsMoveSystem : IEcsInitSystem, IEcsRunSystem, IEcsDestroySystem
    {
        private EcsFilter<CellsMoveEvent> _filter;

        private EcsWorld _world;
        private EcsSystems _subsystems;

        public void Init()
        {
            _subsystems = new EcsSystems(_world);
            _subsystems
                .Add(new CellsMoveUpSubsystem())
                .Add(new CellsMoveDownSubsystem())
                .Add(new CellsMoveRightSubsystem())
                .Add(new CellsMoveLeftSubsystem())
                .OneFrame<CellsMoveEvent>()
                .OneFrame<CellsMoveDownEvent>()
                .OneFrame<CellsMoveUpEvent>()
                .OneFrame<CellsMoveLeftEvent>()
                .OneFrame<CellsMoveRightEvent>()
                .Init();
        }

        public void Run()
        {
            foreach (var i in _filter)
            {
                SetCellsMovable(_filter.Get1(i).StartPosition, _filter.Get1(i).TargetPosition);
                _world.NewEntity().Get<DecreaseAbilityCooldownEvent>().Value = 1;
                AudioManager.PlayAudio(SoundType.PlayerMoveCell);
            }
            
            _subsystems?.Run();
        }

        public void Destroy()
        {
            _subsystems?.Destroy();
        }
        
        private void SetCellsMovable(Vector2Int startPosition, Vector2Int targetPosition)
        {
            var entity = _world.NewEntity();

            if (startPosition.y > targetPosition.y)
                entity.Get<CellsMoveUpEvent>().CellPosition = targetPosition;
            
            if (startPosition.y < targetPosition.y)
                entity.Get<CellsMoveDownEvent>().CellPosition = targetPosition;
            
            if (startPosition.x > targetPosition.x)
                entity.Get<CellsMoveLeftEvent>().CellPosition = targetPosition;

            if (startPosition.x < targetPosition.x)
                entity.Get<CellsMoveRightEvent>().CellPosition = targetPosition;

        }
    }
}