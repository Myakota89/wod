﻿using UnityEngine;

namespace CellsMove.Events
{
    public struct CellsMoveDownEvent
    {
        public Vector2Int CellPosition;
    }
}