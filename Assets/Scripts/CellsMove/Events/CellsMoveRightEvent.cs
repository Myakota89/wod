﻿using UnityEngine;

namespace CellsMove.Events
{
    public struct CellsMoveRightEvent
    {
        public Vector2Int CellPosition;
    }
}