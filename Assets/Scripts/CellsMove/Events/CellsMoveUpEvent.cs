﻿using UnityEngine;

namespace CellsMove.Events
{
    public struct CellsMoveUpEvent
    {
        public Vector2Int CellPosition;
    }
}