﻿using UnityEngine;

namespace CellsMove.Events
{
    public struct CellsMoveEvent
    {
        public Vector2Int StartPosition;
        public Vector2Int TargetPosition;
    }
}