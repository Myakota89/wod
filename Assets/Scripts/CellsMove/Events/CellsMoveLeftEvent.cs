﻿using UnityEngine;

namespace CellsMove.Events
{
    public struct CellsMoveLeftEvent
    {
        public Vector2Int CellPosition;
    }
}