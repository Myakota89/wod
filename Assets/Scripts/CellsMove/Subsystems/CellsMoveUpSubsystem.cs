﻿using CellsMove.Events;
using Common.Components;
using Input.Components;
using Leopotam.Ecs;
using Move;
using Move.Components;
using Spawn.Components;
using UnityEngine;

namespace CellsMove.Subsystems
{
    public class CellsMoveUpSubsystem : IEcsRunSystem
    {
        private EcsFilter<CellsMoveUpEvent> _moveEntity;
        private EcsFilter<UnitComponent> _filter;
        private EcsFilter<InputComponent> _inputEntity;

        private EcsWorld _world;
        
        public void Run()
        {
            foreach (var i in _moveEntity)
            {
                var cellPosition = _moveEntity.Get1(i).CellPosition;
                
                foreach (var idx in _filter)
                {
                    var position = _filter.Get1(idx).Position;
                    
                    if (position.x == cellPosition.x && position.y > cellPosition.y)
                    {
                        ref var movable = ref _filter.GetEntity(idx).Get<MovableEvent>();
                        movable.DestinationPosition = position + Vector2Int.down;
                        movable.Speed = 1f;
                        movable.Callback = () => { _inputEntity.Get1(0).PlayerControl = true; };
                    }
                }
                
                SpawnEnemy(cellPosition.x);
            }
        }

        private void SpawnEnemy(int positionX)
        {
            var gameObject = Resources.Load<GameObject>($"Prefabs/Enemies/Enemy1");
            
            var movable = gameObject.AddComponent<MovableProvider>();
            movable.SetDestinationPosition(new Vector2Int(positionX, 2));
            movable.SetSpeed(1f);
            
            var entity = _world.NewEntity();
            ref var spawnEnemy = ref entity.Get<SpawnEnemyEvent>();
            spawnEnemy.SpawnPosition = new Vector2Int(positionX, 3);
            spawnEnemy.GameObject = gameObject;
        }
    }
}