﻿using Board.Components;
using CellsMove.Events;
using Common.Components;
using GameCamera.Components;
using Input.Components;
using Leopotam.Ecs;
using Move;
using Move.Components;
using Spawn.Components;
using UI.Components;
using Units.Battle.Cell.Components;
using UnityEngine;

namespace CellsMove.Subsystems
{
    public class CellsMoveRightSubsystem : IEcsRunSystem
    {
        private EcsFilter<CellsMoveRightEvent> _moveEntity;
        private EcsFilter<BoardComponent> _boardEntity;
        private EcsFilter<ProgressComponent> _progressEntity;
        private EcsFilter<CameraComponent> _cameraEntity;
        private EcsFilter<InputComponent> _inputEntity;
        private EcsFilter<UnitComponent>.Exclude<CellTag> _filter;
        
        private EcsWorld _world;
        
        public void Run()
        {
            foreach (var i in _moveEntity)
            {
                var destroyCellPosition = _moveEntity.Get1(i).CellPosition;
                
                foreach (var idx in _filter)
                {
                    var cellPosition = _filter.Get1(idx).Position;
                    
                    if (cellPosition.x < destroyCellPosition.x && cellPosition.y == destroyCellPosition.y)
                    {
                        ref var movable = ref _filter.GetEntity(idx).Get<MovableEvent>();
                        movable.DestinationPosition = cellPosition + Vector2Int.right;
                        movable.Speed = 1f;

                        if (_progressEntity.Get1(0).IsEndBoard)
                            movable.Callback = () => _inputEntity.Get1(0).PlayerControl = true;
                        else
                            movable.Callback = SetCameraMove;
                    }
                }

                if (_progressEntity.Get1(0).IsEndBoard)
                    SpawnEnemy(destroyCellPosition.y);
            }
        }

        private void SpawnEnemy(int positionY)
        {
            var gameObject = Resources.Load<GameObject>("Prefabs/Enemies/Enemy1");
            
            var movable = gameObject.AddComponent<MovableProvider>();
            movable.SetDestinationPosition(new Vector2Int(_boardEntity.Get1(0).Width - 3, positionY));
            movable.SetSpeed(1f);
            
            var entity = _world.NewEntity();
            ref var spawnEnemy = ref entity.Get<SpawnEnemyEvent>();
            spawnEnemy.SpawnPosition = new Vector2Int(_boardEntity.Get1(0).Width - 4, positionY);
            spawnEnemy.GameObject = gameObject;
        }

        private void SetCameraMove()
        {
            _progressEntity.Get1(0).CurrentStep++;
            _cameraEntity.GetEntity(0).Get<CameraMoveEvent>().Callback = () => { _inputEntity.Get1(0).PlayerControl = true; };
        }
    }
}