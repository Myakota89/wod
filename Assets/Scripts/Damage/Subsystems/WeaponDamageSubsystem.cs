﻿using Damage.Components;
using Equipments.Weapon;
using Leopotam.Ecs;

namespace Damage.Subsystems
{
    public class WeaponDamageSubsystem : IEcsRunSystem
    {
        private EcsFilter<WeaponComponent, DamageEvent> filter;

        public void Run()
        {
            foreach (var i in filter)
            {
                ref var damage = ref filter.Get2(i).Value;
                ref var weapon = ref filter.Get1(i);
                
                TakeDamage(ref weapon.CurrentArmor, ref damage);
                TakeDamage(ref weapon.CurrentHealth, ref damage);
            }
        }

        private void TakeDamage(ref int currentValue, ref int damage)
        {
            if (currentValue >= damage)
            {
                currentValue -= damage;
                damage = 0;
            }
            else
            {
                damage -= currentValue;
                currentValue = 0;
            }
        }
    }
}