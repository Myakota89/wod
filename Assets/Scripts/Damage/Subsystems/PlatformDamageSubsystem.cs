﻿using Damage.Components;
using Equipments.Platform;
using Leopotam.Ecs;

namespace Damage.Subsystems
{
    public class PlatformDamageSubsystem : IEcsRunSystem
    {
        private EcsFilter<PlatformComponent, DamageEvent> filter;

        public void Run()
        {
            foreach (var i in filter)
            {
                ref var damage = ref filter.Get2(i).Value;
                ref var platform = ref filter.Get1(i);
                
                TakeDamage(ref platform.CurrentArmor, ref damage);
                TakeDamage(ref platform.CurrentHealth, ref damage);
            }
        }

        private void TakeDamage(ref int currentValue, ref int damage)
        {
            if (currentValue >= damage)
            {
                currentValue -= damage;
                damage = 0;
            }
            else
            {
                damage -= currentValue;
                currentValue = 0;
            }
        }
    }
}