﻿using Damage.Components;
using Destroy.Components;
using Equipments.Head;
using Leopotam.Ecs;

namespace Damage.Subsystems
{
    public class HeadDamageSubsystem : IEcsRunSystem
    {
        private EcsFilter<HeadComponent, DamageEvent> filter;

        public void Run()
        {
            foreach (var i in filter)
            {
                ref var damage = ref filter.Get2(i);
                ref var head = ref filter.Get1(i);
                
                TakeDamage(ref head.CurrentArmor, ref damage.Value);
                TakeDamage(ref head.CurrentHealth, ref damage.Value);

                if (!head.IsAlive)
                    filter.GetEntity(i).Get<DestroyEvent>();
            }
        }

        private void TakeDamage(ref int currentValue, ref int damage)
        {
            if (currentValue >= damage)
            {
                currentValue -= damage;
                damage = 0;
            }
            else
            {
                damage -= currentValue;
                currentValue = 0;
            }
        }
    }
}