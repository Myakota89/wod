﻿using Damage.Components;
using Damage.Subsystems;
using Leopotam.Ecs;

namespace Damage.Systems
{
    public class DamageSystem : IEcsInitSystem, IEcsRunSystem, IEcsDestroySystem
    {
        private EcsWorld _world;
        private EcsSystems _subsystems;

        public void Init()
        {
            _subsystems = new EcsSystems(_world);
            _subsystems
                .Add(new WeaponDamageSubsystem())
                .Add(new PlatformDamageSubsystem())
                .Add(new HeadDamageSubsystem())
                .OneFrame<DamageEvent>()
                .Init();
        }

        public void Run()
        {
            _subsystems?.Run();
        }
        
        public void Destroy()
        {
            _subsystems?.Destroy();
        }
    }
}