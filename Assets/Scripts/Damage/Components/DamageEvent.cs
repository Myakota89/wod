﻿using Enum;

namespace Damage.Components
{
    public struct DamageEvent
    {
        public int Value;
        public DamageType Type;
    }
}