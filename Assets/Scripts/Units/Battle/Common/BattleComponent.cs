﻿namespace Units.Battle.Common
{
    public struct BattleComponent
    {
        public int Armor;
        public int ArmorSetValue;
        public int Health;
        public int HealthSetValue;
    }
}