﻿using System;
using UnityEngine;

namespace Units.Collect.Lamp.Components
{
    [Serializable]
    public struct LampComponent
    {
        public SpriteRenderer SpriteRenderer;
        public Sprite LampSprite;
        public Sprite Lamp10Sprite;
        public int LampCount;
    }
}