﻿using Units.Collect.Lamp.Components;
using Voody.UniLeo;

namespace Units.Collect.Lamp.Providers
{
    public class LampProvider : MonoProvider<LampComponent>
    {
        public void SetLampCount(int lampCount)
        {
            value.LampCount = lampCount;
            value.SpriteRenderer.sprite = lampCount >= 10 ? value.Lamp10Sprite : value.LampSprite;
        }
    }
}