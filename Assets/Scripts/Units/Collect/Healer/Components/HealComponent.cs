﻿using System;

namespace Units.Collect.Healer.Components
{
    [Serializable]
    public struct HealComponent
    {
        public int HealthHeal;
        public int ArmorHeal;
    }
}