﻿using Units.Collect.Healer.Components;
using Voody.UniLeo;

namespace Units.Collect.Healer.Providers
{
    public class HealProvider : MonoProvider<HealComponent>
    {
        public void SetArmorValue(int armor) =>
            value.ArmorHeal = armor;

        public void SetHealthValue(int health) =>
            value.HealthHeal = health;
    }
}