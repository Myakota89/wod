﻿using System;
using UnityEngine.UI;

namespace UI.Components
{
    [Serializable]
    public struct ProgressComponent
    {
        public Image ProgressSlider; 
        public int CurrentStep;
        public bool IsEndBoard;
    }
}