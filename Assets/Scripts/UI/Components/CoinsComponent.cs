﻿using System;
using TMPro;

namespace UI.Components
{
    [Serializable]
    public struct CoinsComponent
    {
        public TextMeshProUGUI CoinsText;
        public int Count;
    }
}