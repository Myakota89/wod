﻿namespace UI.Events
{
    public struct CoinsIncreaseEvent
    {
        public int Value;
    }
}