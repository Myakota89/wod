﻿using Leopotam.Ecs;
using UI.Events;

namespace UI.Systems
{
    public class UISystem : IEcsInitSystem, IEcsRunSystem, IEcsDestroySystem
    {
        private EcsWorld _world;
        private EcsSystems _subsystems;

        public void Init()
        {
            _subsystems = new EcsSystems(_world);
            _subsystems
                .Add(new ProgressSubsystem())
                .Add(new CoinsSubsystem())
                .OneFrame<CoinsIncreaseEvent>()
                .Init();
        }

        public void Run()
        {
            _subsystems?.Run();
        }

        public void Destroy()
        {
            _subsystems?.Destroy();
        }
    }
}