﻿using Board.Components;
using Leopotam.Ecs;
using UI.Components;

namespace UI.Systems
{
    public class ProgressSubsystem : IEcsRunSystem
    {
        private EcsFilter<ProgressComponent> progressEntity;
        private EcsFilter<BoardComponent> boardEntity;

        public void Run()
        {
            ref var progress = ref progressEntity.Get1(0);
            var maxStep = boardEntity.Get1(0).Width - 3;
            progress.ProgressSlider.fillAmount = (float)progress.CurrentStep / maxStep;
            progress.IsEndBoard = progress.CurrentStep >= maxStep;
        }
    }
}