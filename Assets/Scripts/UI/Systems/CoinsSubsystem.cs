﻿using Leopotam.Ecs;
using UI.Components;
using UI.Events;

namespace UI.Systems
{
    public class CoinsSubsystem : IEcsRunSystem
    {
        private EcsFilter<CoinsComponent> _coinsEntity;
        private EcsFilter<CoinsIncreaseEvent> _filter;

        public void Run()
        {
            ref var coins = ref _coinsEntity.Get1(0);
            coins.CoinsText.text = coins.Count.ToString();

            foreach (var i in _filter)
            {
                coins.Count += _filter.Get1(i).Value;
            }
        }
    }
}