﻿using Enum;
using UnityEngine;

public class EntityInformant : MonoBehaviour
{
    public int posX;
    public int posY;
    public UnitState State;
}