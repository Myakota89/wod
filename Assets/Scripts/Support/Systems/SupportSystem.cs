﻿using Heal;
using Heal.Systems;
using Leopotam.Ecs;

namespace Support.Systems
{
    public class SupportSystem : IEcsInitSystem, IEcsRunSystem, IEcsDestroySystem
    {
        private EcsWorld _world;
        private EcsSystems _subsystems;
        
        public void Init()
        {
            _subsystems = new EcsSystems(_world);
            _subsystems
                .Add(new HealSystem())
                .Init();
        }

        public void Run()
        {
            _subsystems?.Run();
        }

        public void Destroy()
        {
            _subsystems?.Destroy();
        }
    }
}