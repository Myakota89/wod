﻿using Equipments.Weapon;
using Leopotam.Ecs;
using Recovery.Events;

namespace Recovery.Subsystems
{
    public class WeaponRecoverySubsystem : IEcsRunSystem
    {
        private EcsFilter<WeaponComponent, WeaponRecoveryEvent> _filter;

        public void Run()
        {
            foreach (var i in _filter)
            {
                ref var weapon = ref _filter.Get1(i);
                ref var recovery = ref _filter.Get2(i);

                weapon.CurrentHealth += recovery.Health;
                if (weapon.CurrentHealth > weapon.MaxHealth)
                    weapon.CurrentHealth = weapon.MaxHealth;
                
                weapon.CurrentArmor += recovery.Armor;
                if (weapon.CurrentArmor > weapon.MaxArmor)
                    weapon.CurrentArmor = weapon.MaxArmor;
            }
        }
    }
}