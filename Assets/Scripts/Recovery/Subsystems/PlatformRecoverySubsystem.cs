﻿using Equipments.Platform;
using Leopotam.Ecs;
using Recovery.Events;

namespace Recovery.Subsystems
{
    public class PlatformRecoverySubsystem : IEcsRunSystem
    {
        private EcsFilter<PlatformComponent, PlatformRecoveryEvent> _filter;

        public void Run()
        {
            foreach (var i in _filter)
            {
                ref var platform = ref _filter.Get1(i);
                ref var recovery = ref _filter.Get2(i);

                platform.CurrentHealth += recovery.Health;
                if (platform.CurrentHealth > platform.MaxHealth)
                    platform.CurrentHealth = platform.MaxHealth;
                
                platform.CurrentArmor += recovery.Armor;
                if (platform.CurrentArmor > platform.MaxArmor)
                    platform.CurrentArmor = platform.MaxArmor;
            }
        }
    }
}