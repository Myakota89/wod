﻿using Audio;
using Core;
using Equipments.Head;
using Leopotam.Ecs;
using Recovery.Events;

namespace Recovery.Subsystems
{
    public class HeadRecoverySubsystem : IEcsRunSystem
    {
        private EcsFilter<HeadComponent, HeadRecoveryEvent> _filter;

        public void Run()
        {
            foreach (var i in _filter)
            {
                ref var head = ref _filter.Get1(i);
                ref var recovery = ref _filter.Get2(i);

                if (recovery.Health > 0)
                    AudioManager.PlayAudio(SoundType.HealthRefill);

                if (recovery.Armor > 0)
                    AudioManager.PlayAudio(SoundType.ArmorRefill);

                head.CurrentHealth += recovery.Health;
                if (head.CurrentHealth > head.MaxHealth)
                    head.CurrentHealth = head.MaxHealth;
                
                head.CurrentArmor += recovery.Armor;
                if (head.CurrentArmor > head.MaxArmor)
                    head.CurrentArmor = head.MaxArmor;
            }
        }
    }
}