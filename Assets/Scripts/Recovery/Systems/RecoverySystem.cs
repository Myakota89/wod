﻿using Leopotam.Ecs;
using Recovery.Events;
using Recovery.Subsystems;

namespace Recovery.Systems
{
    public class RecoverySystem : IEcsInitSystem, IEcsRunSystem, IEcsDestroySystem
    {
        private EcsWorld _world;
        private EcsSystems _subsystems;
        
        public void Init()
        {
            _subsystems = new EcsSystems(_world);
            _subsystems
                .Add(new HeadRecoverySubsystem())
                .Add(new PlatformRecoverySubsystem())
                .Add(new WeaponRecoverySubsystem())
                .OneFrame<HeadRecoveryEvent>()
                .OneFrame<PlatformRecoveryEvent>()
                .OneFrame<WeaponRecoveryEvent>()
                .Init();
        }

        public void Run()
        {
            _subsystems?.Run();
        }

        public void Destroy()
        {
            _subsystems?.Destroy();
        }
    }
}