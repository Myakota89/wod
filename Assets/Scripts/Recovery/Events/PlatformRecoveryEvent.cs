﻿namespace Recovery.Events
{
    public struct PlatformRecoveryEvent
    {
        public int Health;
        public int Armor;
    }
}