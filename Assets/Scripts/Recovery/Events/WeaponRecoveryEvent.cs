﻿namespace Recovery.Events
{
    public struct WeaponRecoveryEvent
    {
        public int Health;
        public int Armor;
    }
}