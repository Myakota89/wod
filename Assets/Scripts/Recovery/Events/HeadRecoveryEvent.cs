﻿namespace Recovery.Events
{
    public struct HeadRecoveryEvent
    {
        public int Health;
        public int Armor;
    }
}