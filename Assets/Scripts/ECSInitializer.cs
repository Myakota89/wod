﻿using Abilities;
using Animation.Systems;
using Audio.Systems;
using Board.Events;
using Board.Systems;
using CellsMove.Systems;
using Damage.Systems;
using Destroy.Systems;
using Enum;
using Equipments.Systems;
using GameCamera.Systems;
using Heal.Systems;
using Input.Components;
using Input.Systems;
using Leopotam.Ecs;
using Leopotam.Ecs.Ui.Systems;

#if UNITY_EDITOR
using Leopotam.Ecs.UnityIntegration;
#endif

using Move.Systems;
using Popups;
using Recovery.Systems;
using ScriptsSO;
using Spawn.Systems;
using State;
using UI.Systems;
using UnityEngine;
using View.Systems;
using Voody.UniLeo;

public class ECSInitializer : MonoBehaviour
{
    [SerializeField] private ResourceManager resourceManager;
    [SerializeField] private Settings settings;
    [SerializeField] private EcsUiEmitter uiEmitter;

    private EcsWorld world;
    private EcsSystems systems;

    private void Start()
    {
        Screen.orientation = ScreenOrientation.Portrait;
        resourceManager.Init();
        settings.Init();
        PopupManager.SetPopupParent();

        world = new EcsWorld();
        
#if UNITY_EDITOR
        EcsWorldObserver.Create (world);
#endif 
        
        systems = new EcsSystems(world);

        systems
            .Add(new InputSystem(), nameof(InputSystem))
            .Add(new StateSystem())
            .Add(new BoardGeneratorSystem())
            .Add(new SpawnSystem())
            .Add(new MovableSystem())
            .Add(new AnimationSystem())
            .Add(new DamageSystem())
            .Add(new CameraMoveSystem(), nameof(CameraMoveSystem))
            .Add(new DestroySystem())
            .Add(new EquipmentSystem())
            .Add(new AbilitySystem())
            .Add(new HealSystem())
            .Add(new RecoverySystem())
            .Add(new CellsMoveSystem())
            .Add(new ViewSystem())
            .Add(new AnimationSystem())
            .Add(new AudioSystem())
            .Add(new UISystem())
            .Add(new PopupSystem())
            .ConvertScene()
            .OneFrame<GenerateBoardEvent>()
            .OneFrame<SimpleTapEvent>()
            .InjectUi(uiEmitter)
            .Init();

#if UNITY_EDITOR
        EcsSystemsObserver.Create (systems);
#endif
        
    }

    private void Update()
    {
        systems.SetRunSystemState(systems.GetNamedRunSystem(nameof(InputSystem)), Game.TurnState == TurnState.AwaitInput);
        systems.SetRunSystemState(systems.GetNamedRunSystem(nameof(CameraMoveSystem)), Game.TurnState == TurnState.CameraMoving);
        
        systems.Run();
    }

    private void OnDestroy()
    {
        systems.Destroy();
        world.Destroy();
    }
}