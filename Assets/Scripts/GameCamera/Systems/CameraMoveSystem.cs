﻿using Destroy.Components;
using DG.Tweening;
using GameCamera.Components;
using Leopotam.Ecs;
using ScriptsSO;

namespace GameCamera.Systems
{
    public class CameraMoveSystem : IEcsInitSystem, IEcsRunSystem, IEcsDestroySystem
    {
        private EcsFilter<CameraComponent, CameraMoveEvent> _filter;
        
        private EcsWorld _world;
        private EcsSystems _subsystems;

        public void Init()
        {
            _subsystems = new EcsSystems(_world);
            _subsystems
                .OneFrame<CameraMoveEvent>()
                .Init();
        }

        public void Run()
        {
            foreach (var i in _filter)
            {
                ref var camera = ref _filter.Get1(i);
                var callback = _filter.Get2(i).Callback;

                camera.Transform.DOMoveX(camera.Transform.position.x + 1,
                    Settings.BoardSpeed).OnComplete(() =>
                {
                   callback?.Invoke();
                    _world.NewEntity().Get<BoardFrameDestroyEvent>();
                });
            }
            
            _subsystems?.Run();
        }

        public void Destroy()
        {
            _subsystems?.Destroy();
        }
    }
}
