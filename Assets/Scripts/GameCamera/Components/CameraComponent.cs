﻿using System;
using UnityEngine;

namespace GameCamera.Components
{
    [Serializable]
    public struct CameraComponent
    {
        public UnityEngine.Camera Camera;
        public Transform Transform;
    }
}