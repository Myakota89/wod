﻿using System;

namespace GameCamera.Components
{
    public struct CameraMoveEvent
    {
        public Action Callback;
    }
}