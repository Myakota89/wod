﻿using Common.Components;
using DG.Tweening;
using Leopotam.Ecs;
using Move.Components;
using UnityEngine;

namespace Move.Systems
{
    public class MovableSystem : IEcsInitSystem, IEcsRunSystem, IEcsDestroySystem
    {
        private EcsFilter<UnitComponent, ViewComponent, MovableEvent> _filter;

        private EcsWorld _world;
        private EcsSystems _subsystems;

        public void Init()
        {
            _subsystems = new EcsSystems(_world);
            _subsystems
                .OneFrame<MovableEvent>()
                .Init();
        }

        public void Run()
        {
            foreach (var i in _filter)
            {
                ref var view = ref _filter.Get2(i);
                ref var move = ref _filter.Get3(i);
                var callback = move.Callback;
                _filter.Get1(i).Position = _filter.Get3(i).DestinationPosition;

                var targetPosition = new Vector3(move.DestinationPosition.x, -move.DestinationPosition.y);
                view.Transform.DOMove(targetPosition, move.Speed).OnComplete(() =>
                {
                    callback?.Invoke();
                });
            }
            
            _subsystems?.Run();
        }

        public void Destroy()
        {
            _subsystems?.Destroy();
        }
    }
}