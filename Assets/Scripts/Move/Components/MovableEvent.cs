﻿using System;
using UnityEngine;

namespace Move.Components
{
    [Serializable]
    public struct MovableEvent
    {
        public float Speed;
        public Vector2Int DestinationPosition;
        public Action Callback;
    }
}