﻿using Move.Components;
using UnityEngine;
using Voody.UniLeo;

namespace Move
{
    public class MovableProvider : MonoProvider<MovableEvent>
    {
        public void SetDestinationPosition(Vector2Int position) =>
            value.DestinationPosition = position;

        public void SetSpeed(float speed) =>
            value.Speed = speed;
    }
}