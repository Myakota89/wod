﻿using Animation.Damage.Components;
using Animation.Damage.Subsystems;
using Animation.Destroy.Components;
using Animation.Destroy.Subsystems;
using Animation.Spawn.Components;
using Animation.Spawn.Subsystems;
using Leopotam.Ecs;

namespace Animation.Systems
{
    public class AnimationSystem : IEcsInitSystem, IEcsRunSystem, IEcsDestroySystem
    {
        private EcsWorld _world;
        private EcsSystems _subsystems;
        
        public void Init()
        {
            _subsystems = new EcsSystems(_world);
            _subsystems
                .Add(new DamageAnimationSubsystem())
                .Add(new DestroyAnimationSubsystem())
                .Add(new SpawnAnimationSubsystem())
                .OneFrame<DamageAnimationEvent>()
                .OneFrame<DestroyAnimationEvent>()
                .OneFrame<SpawnAnimationEvent>()
                .Init();
        }

        public void Run()
        {
            _subsystems?.Run();
        }

        public void Destroy()
        {
            _subsystems?.Destroy();
        }
    }
}