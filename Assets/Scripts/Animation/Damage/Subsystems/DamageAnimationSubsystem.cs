﻿using Animation.Damage.Components;
using Common.Components;
using DG.Tweening;
using Leopotam.Ecs;
using Player;
using UnityEngine;

namespace Animation.Damage.Subsystems
{
    public class DamageAnimationSubsystem : IEcsRunSystem
    {
        private EcsFilter<ViewComponent, DamageAnimationEvent> _filter;
        private EcsFilter<PlayerTag, UnitComponent, ViewComponent> _playerEntity;

        public void Run()
        {
            foreach (var i in _filter)
            {
                var callback = _filter.Get2(i).Callback;
                
                foreach (var renderer in _playerEntity.Get3(0).Transform.GetComponentsInChildren<Renderer>())
                    renderer.sortingLayerName = "Attack";
                
                var animation = GetDamageAnimation(_filter.Get1(i).Transform);
                animation.OnComplete(() =>
                {
                    callback?.Invoke();
                    foreach (var renderer in _playerEntity.Get3(0).Transform.GetComponentsInChildren<Renderer>())
                        renderer.sortingLayerName = "Cell";
                });
            }
        }

        private Sequence GetDamageAnimation(Transform enemy)
        {
            var playerTransform = _playerEntity.Get3(0).Transform;
            var playerPosition = new Vector3(_playerEntity.Get2(0).Position.x, -_playerEntity.Get2(0).Position.y);
            
            var sequence = DOTween.Sequence();
            
            var targetPosition = enemy.position - (enemy.position - playerTransform.position)/2;

            sequence
                .Append(playerTransform.DOScale(1.1f, 0.5f))
                .Append(playerTransform.DOMove(targetPosition, 0.1f))
                .Append(playerTransform.DOLocalMove(playerPosition, 0.5f))
                .Join(enemy.DOShakePosition(0.5f, 0.1f))
                .Append(playerTransform.DOScale(1f, 0.5f));
            
            return sequence;
        }
    }
}