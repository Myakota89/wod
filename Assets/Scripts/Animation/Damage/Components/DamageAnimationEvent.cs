﻿using System;

namespace Animation.Damage.Components
{
    public struct DamageAnimationEvent
    {
        public Action Callback;
    }
}