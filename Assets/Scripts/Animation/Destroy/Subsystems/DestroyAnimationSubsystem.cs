﻿using Animation.Destroy.Components;
using Common.Components;
using DG.Tweening;
using Leopotam.Ecs;
using UnityEngine;

namespace Animation.Destroy.Subsystems
{
    public class DestroyAnimationSubsystem : IEcsRunSystem
    {
        private EcsFilter<ViewComponent, DestroyAnimationEvent> _filter;

        public void Run()
        {
            foreach (var i in _filter)
            {
                ref var view = ref _filter.Get1(i);
                var callback = _filter.Get2(i).Callback;

                foreach (var renderer in view.Transform.GetComponentsInChildren<Renderer>())
                    renderer.sortingLayerName = "DeadCell";
                    
                view.CellSprite.sortingOrder = -1;

                view.Transform.DOScale(0, 0.5f).OnComplete(() =>
                {
                    GameObject.DestroyImmediate(_filter.Get1(i).Transform.gameObject);
                    callback?.Invoke();
                    _filter.GetEntity(i).Destroy();
                });
            }
        }
    }
}