﻿using System;

namespace Animation.Destroy.Components
{
    public struct DestroyAnimationEvent
    {
        public Action Callback;
    }
}