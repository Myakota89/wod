﻿using Animation.Spawn.Components;
using Common.Components;
using DG.Tweening;
using Input.Components;
using Leopotam.Ecs;
using UnityEngine;

namespace Animation.Spawn.Subsystems
{
    public class SpawnAnimationSubsystem : IEcsRunSystem
    {
        private EcsFilter<ViewComponent, SpawnAnimationEvent> _filter;
        private EcsFilter<InputComponent> _inputEntity;

        public void Run()
        {
            foreach (var i in _filter)
            {
                ref var view = ref _filter.Get1(i);

                view.Transform.localScale = Vector3.zero;
                view.Transform.DOScale(1, 0.5f).OnComplete(() =>
                {
                    _inputEntity.Get1(0).PlayerControl = true;
                });
            }
        }
    }
}