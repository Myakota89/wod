﻿using UnityEngine;

namespace ScriptsSO
{
    [CreateAssetMenu(menuName = "Data/Settings")]
    public class Settings : ScriptableObject
    {
        public static Settings Instance;
    
        [SerializeField] private int widthBoard;
        [SerializeField] private int heightBoard;
        [SerializeField] private float playerSpeed;
        [SerializeField] private float boardSpeed;
        [SerializeField] [Range(0, 100)] private int chanceEnemySpawn;
        [SerializeField] private int level;
    
        public float musicVolume = 0.0f;
        public float soundVolume = 0.0f;
        public static int WidthBoard => Instance.widthBoard;
        public static int HeightBoard => Instance.heightBoard;
        public static float PlayerSpeed => Instance.playerSpeed;
        public static float BoardSpeed => Instance.boardSpeed;
        public static float ChanceEnemySpawn => (float) Instance.chanceEnemySpawn / 100;
        public static int Level => Instance.level;

        public void SetLevel(int value)
        {
            level = value;
        }

        public void Init()
        {
            Instance = this;
        }
    }
}
