﻿using Abilities.HealEquipmentAbility.Events;
using Leopotam.Ecs;
using UnityEngine;

namespace ScriptsSO.Ability.Active.Heal
{
    [CreateAssetMenu(menuName = "Ability/Active/Equipment heal")]
    public class HealAbility : ActiveAbility
    {
        public int Health;
        public int Armor;

        public override void OnValidate()
        {
            base.OnValidate();
            Mathf.Clamp(Health, 0, 50);
            Mathf.Clamp(Armor, 0, 50);
        }

        public override void SetComponent(EcsEntity entity)
        {
            entity.Get<HealAbilityEvent>().HealthHeal = Health;
            entity.Get<HealAbilityEvent>().ArmorHeal = Armor;
            entity.Get<HealAbilityEvent>().Ability = this;
        }
    }
}