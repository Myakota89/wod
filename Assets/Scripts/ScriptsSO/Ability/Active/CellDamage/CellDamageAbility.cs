﻿using Abilities.CellDamageAbility.Events;
using Leopotam.Ecs;
using UnityEngine;

namespace ScriptsSO.Ability.Active.Implementation
{
    [CreateAssetMenu(menuName = "Ability/Active/Cell Damage")]
    public class CellDamageAbility : ActiveAbility
    {
        public int Damage;
        
        public override void SetComponent(EcsEntity entity)
        {
            entity.Get<CellDamageAbilityEvent>().Damage = Damage;
            entity.Get<CellDamageAbilityEvent>().ActiveAbility = this;
        }
    }
}