﻿using Abilities.RecoveryAbility.Events;
using Leopotam.Ecs;
using UnityEngine;

namespace ScriptsSO.Ability.Active.Recovery
{
    [CreateAssetMenu(menuName = "Ability/Active/Health recovery")]
    public class HealthRecoveryAbility : ActiveAbility
    {
        public int Health;

        public override void OnValidate()
        {
            base.OnValidate();
            Mathf.Clamp(Health, 0, 50);
        }

        public override void SetComponent(EcsEntity entity)
        {
            entity.Get<HealthRecoveryAbilityEvent>().Health = Health;
            entity.Get<HealthRecoveryAbilityEvent>().Ability = this;
        }
    }
}