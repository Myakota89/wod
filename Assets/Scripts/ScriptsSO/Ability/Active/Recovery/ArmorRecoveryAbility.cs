﻿using Abilities.RecoveryAbility.Events;
using Leopotam.Ecs;
using UnityEngine;

namespace ScriptsSO.Ability.Active.Recovery
{
    [CreateAssetMenu(menuName = "Ability/Active/Armor recovery")]
    public class ArmorRecoveryAbility : ActiveAbility
    {
        public int Armor;

        public override void OnValidate()
        {
            base.OnValidate();
            Mathf.Clamp(Armor, 0, 50);
        }

        public override void SetComponent(EcsEntity entity)
        {
            entity.Get<ArmorRecoveryAbilityEvent>().Armor = Armor;
            entity.Get<ArmorRecoveryAbilityEvent>().Ability = this;
        }
    }
}