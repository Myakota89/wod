﻿using Leopotam.Ecs;
using UnityEngine;

namespace ScriptsSO.Ability.Active
{
    public abstract class ActiveAbility : ScriptableObject
    {
        public TargetType Type;
        public int Cooldown;
        public Sprite AbilitySprite;
        
        public int CurrentCooldown { get; private set; }

        public virtual void OnValidate()
        {
            if (Cooldown < 0)
                Cooldown = 0;
        }

        public abstract void SetComponent(EcsEntity entity);

        public void Reload() => 
            CurrentCooldown = Cooldown;

        public bool IsReady() => 
            CurrentCooldown == 0;

        public void DecreaseCooldown(int value = 1)
        {
            CurrentCooldown -= value;
            if (CurrentCooldown < 0)
                CurrentCooldown = 0;
        }

    }
}