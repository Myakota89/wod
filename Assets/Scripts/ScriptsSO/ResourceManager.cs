﻿using UnityEngine;

namespace ScriptsSO
{
    [CreateAssetMenu(menuName = "Data/Resource Manager")]
    public class ResourceManager : ScriptableObject
    {
        public static ResourceManager Instance;
        
        [SerializeField] private EquipmentsData EquipmentsData;

        private EquipmentsData _equipments;

        public static EquipmentsData Equipments
        {
            get
            {
                if (Instance._equipments == null)
                    Instance._equipments = Instantiate(Instance.EquipmentsData);

                return Instance._equipments;
            }
        }

        public void Init()
        {
            Instance = this;
        }
    }
}