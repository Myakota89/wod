﻿using Enum;
using Equipments.Head;
using ScriptsSO.Ability.Active;
using UnityEngine;

namespace ScriptsSO.Equipments
{
    [CreateAssetMenu(menuName = "Data/Equipment/Head")]
    public class HeadData : Equipment
    {
        [SerializeField] private HeadType type;
        [SerializeField] private ActiveAbility activeAbility;
        [SerializeField] private Sprite headSprite;

        public HeadType Type => type;
        public HeadComponent GetHeadComponent()
        {
            var component = new HeadComponent
            {
                CurrentArmor = armor,
                MaxArmor = armor,
                CurrentHealth = health,
                MaxHealth = health,
                HeadType = type,
                ActiveAbility = activeAbility != null ? Instantiate(activeAbility) : null,
                Sprite = headSprite
            };

            return component;
        }
    }
}