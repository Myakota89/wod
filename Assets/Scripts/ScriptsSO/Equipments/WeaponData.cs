﻿using Enum;
using Equipments.Weapon;
using ScriptsSO.Ability.Active;
using UnityEngine;

namespace ScriptsSO.Equipments
{
    [CreateAssetMenu(menuName = "Data/Equipment/Weapon")]
    public class WeaponData : Equipment
    {
        [SerializeField] private WeaponType type;
        [SerializeField] private ActiveAbility activeAbility;
        [SerializeField] private Sprite leftWeaponSprite;
        [SerializeField] private Sprite rightWeaponSprite;

        public WeaponType Type => type;

        public WeaponComponent GetWeaponComponent()
        {
            var component = new WeaponComponent
            {
                CurrentArmor = armor,
                MaxArmor = armor,
                CurrentHealth = health,
                MaxHealth = health,
                WeaponType = type,
                ActiveAbility = activeAbility != null ? Instantiate(activeAbility) : null,
                LeftWeaponSprite = leftWeaponSprite,
                RightWeaponSprite = rightWeaponSprite
            };

            return component;
        }
    }
}