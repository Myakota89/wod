﻿using Enum;
using Equipments.Platform;
using ScriptsSO.Ability.Active;
using UnityEngine;

namespace ScriptsSO.Equipments
{
    [CreateAssetMenu(menuName = "Data/Equipment/Platform")]
    public class PlatformData : Equipment
    {
        [SerializeField] private PlatformType type;
        [SerializeField] private ActiveAbility activeAbility;
        [SerializeField] private Sprite platformSprite;

        public PlatformType Type => type;
        public PlatformComponent GetPlatformComponent()
        {
            var component = new PlatformComponent
            {
                CurrentArmor = armor,
                MaxArmor = armor,
                CurrentHealth = health,
                MaxHealth = health,
                PlatformType = type,
                ActiveAbility = activeAbility != null ? Instantiate(activeAbility) : null,
                Sprite = platformSprite
            };

            return component;
        }
    }
}