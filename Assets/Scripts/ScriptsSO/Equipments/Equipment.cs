﻿using UnityEngine;

namespace ScriptsSO.Equipments
{
    public abstract class Equipment : ScriptableObject
    {
        [SerializeField] protected int health;
        [SerializeField] protected int armor;
    }
}