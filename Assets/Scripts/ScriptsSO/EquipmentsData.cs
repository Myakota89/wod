using System.Collections.Generic;
using System.Linq;
using Enum;
using Extensions;
using ScriptsSO.Equipments;
using UnityEngine;

namespace ScriptsSO
{
    [CreateAssetMenu(menuName = "Data/Equipment settings")]
    public class EquipmentsData : ScriptableObject
    {
        [Header("Start Player Equipment")] 
        public WeaponData startWeaponData;
        public HeadData startHeadData;
        public PlatformData startPlatformData;

        public WeaponData StartWeaponData => startWeaponData;
        public HeadData StartHeadData => startHeadData;
        public PlatformData StartPlatformData => startPlatformData;

        [Space(10)]
        public List<WeaponData> WeaponData;
        public List<HeadData> HeadData;
        public List<PlatformData> PlatformData;

        public WeaponData GetWeaponDataExclude(WeaponType type) =>
            WeaponData.Where(d => d.Type != type).ToList().GetRandom();
        
        public HeadData GetHeadDataExclude(HeadType type) =>
            HeadData.Where(d => d.Type != type).ToList().GetRandom();
        
        public PlatformData GetPlatformDataExclude(PlatformType type) =>
            PlatformData.Where(d => d.Type != type).ToList().GetRandom();

    }
}
