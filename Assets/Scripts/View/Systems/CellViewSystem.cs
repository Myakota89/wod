﻿using Animation;
using Animation.Destroy.Components;
using Common.Components;
using Leopotam.Ecs;

namespace View.Systems
{
    public class CellViewSystem : IEcsRunSystem
    {
        private EcsFilter<UnitComponent, ViewComponent>.Exclude<DestroyAnimationEvent> _filter;

        public void Run()
        {
            foreach (var i in _filter)
            {
                ref var unit = ref _filter.Get1(i);
                ref var view = ref _filter.Get2(i);

                view.CellSprite.sortingOrder = unit.Position.y;
            }
        }
    }
}