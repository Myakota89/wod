﻿using Leopotam.Ecs;

namespace View.Systems
{
    public class ViewSystem : IEcsInitSystem, IEcsRunSystem, IEcsDestroySystem
    {
        private EcsWorld _world;
        private EcsSystems _subsystems;
        
        public void Init()
        {
            _subsystems = new EcsSystems(_world);
            _subsystems
                .Add(new CellViewSystem())
                .Init();
        }

        public void Run()
        {
            _subsystems?.Run();
        }

        public void Destroy()
        {
            _subsystems?.Destroy();
        }
    }
}