﻿using Heal.Events;
using Heal.Subsystems;
using Leopotam.Ecs;

namespace Heal.Systems
{
    public class HealSystem : IEcsInitSystem, IEcsRunSystem, IEcsDestroySystem
    {
        private EcsWorld _world;
        private EcsSystems _subsystems;

        public void Init()
        {
            _subsystems = new EcsSystems(_world);
            _subsystems
                .Add(new HeadHealSubsystem())
                .Add(new PlatformHealSubsystem())
                .Add(new WeaponHealSubsystem())
                .Add(new UnitHealStatsSubsystem())
                .OneFrame<HealEvent>()
                .Init();
        }

        public void Run()
        {
            _subsystems?.Run();
        }

        public void Destroy()
        {
            _subsystems?.Destroy();
        }
    }
}