﻿using Equipments.Platform;
using Heal.Events;
using Leopotam.Ecs;
using Recovery.Events;

namespace Heal.Subsystems
{
    public class PlatformHealSubsystem : IEcsRunSystem
    {
        private EcsFilter<PlatformComponent, HealEvent> _filter;

        public void Run()
        {
            foreach (var i in _filter)
            {
                ref var head = ref _filter.Get1(i);
                ref var heal = ref _filter.Get2(i);

                _filter.GetEntity(i).Get<PlatformRecoveryEvent>().Health = heal.HealthHeal;
                CalculateHeal(head.MaxHealth, head.CurrentHealth, ref heal.HealthHeal);
                
                _filter.GetEntity(i).Get<PlatformRecoveryEvent>().Armor = heal.ArmorHeal;
                CalculateHeal(head.MaxArmor, head.CurrentArmor, ref heal.ArmorHeal);
            }
        }
        
        private void CalculateHeal(int max, int current, ref int heal)
        {
            if (max - current >= heal)
                heal = 0;
            else
                heal -= (max - current);
        }
    }
}