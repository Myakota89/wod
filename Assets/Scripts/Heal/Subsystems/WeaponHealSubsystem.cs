﻿using Equipments.Weapon;
using Heal.Events;
using Leopotam.Ecs;
using Recovery.Events;

namespace Heal.Subsystems
{
    public class WeaponHealSubsystem : IEcsRunSystem
    {
        private EcsFilter<WeaponComponent, HealEvent> _filter;

        public void Run()
        {
            foreach (var i in _filter)
            {
                ref var head = ref _filter.Get1(i);
                ref var heal = ref _filter.Get2(i);

                _filter.GetEntity(i).Get<WeaponRecoveryEvent>().Health = heal.HealthHeal;
                CalculateHeal(head.MaxHealth, head.CurrentHealth, ref heal.HealthHeal);
                
                _filter.GetEntity(i).Get<WeaponRecoveryEvent>().Armor = heal.ArmorHeal;
                CalculateHeal(head.MaxArmor, head.CurrentArmor, ref heal.ArmorHeal);
            }
        }
        
        private void CalculateHeal(int max, int current, ref int heal)
        {
            if (max - current >= heal)
                heal = 0;
            else
                heal -= (max - current);
        }
    }
}