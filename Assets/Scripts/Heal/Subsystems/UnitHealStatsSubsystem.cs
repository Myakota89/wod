﻿using Equipments.Common;
using Leopotam.Ecs;
using Units.Collect.Healer.Components;

namespace Heal.Subsystems
{
    public class UnitHealStatsSubsystem : IEcsRunSystem
    {
        private EcsFilter<HealTag, HealComponent, StatsViewComponent> _filter;
        
        public void Run()
        {
            foreach (var i in _filter)
            {
                ref var heal = ref _filter.Get2(i);
                ref var view = ref _filter.Get3(i);

                view.ArmorText.text = $"+{heal.ArmorHeal.ToString()}";
                view.HealthText.text = $"+{heal.HealthHeal.ToString()}";

                view.ArmorSprite.gameObject.SetActive(heal.ArmorHeal > 0);
                view.HealthSprite.gameObject.SetActive(heal.HealthHeal > 0);
            }
        }
    }
}