﻿namespace Heal.Events
{
    public struct HealEvent
    {
        public int HealthHeal;
        public int ArmorHeal;
    }
}