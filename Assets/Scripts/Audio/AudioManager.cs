﻿using Audio.Components;
using Core;
using Leopotam.Ecs;
using Voody.UniLeo;

namespace Audio
{
    public static class AudioManager
    {
        public static void PlayAudio(SoundType soundType)
        {
            WorldHandler.GetWorld().NewEntity().Get<SoundClipEvent>().SoundType = soundType;;
        }
    }
}