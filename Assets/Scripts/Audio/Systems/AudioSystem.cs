﻿using Audio.Components;
using Leopotam.Ecs;

namespace Audio.Systems
{
    public class AudioSystem : IEcsInitSystem, IEcsRunSystem, IEcsDestroySystem
    {
        private EcsFilter<AudioComponent> _audioEntity;
        private EcsFilter<SoundClipEvent> filter;

        private EcsWorld _world;
        private EcsSystems _subsystems;

        public void Init()
        {
            _subsystems = new EcsSystems(_world);
            _subsystems
                .OneFrame<SoundClipEvent>()
                .Init();
        }

        public void Run()
        {
            foreach (var i in filter)
            {
                ref var audioController = ref _audioEntity.Get1(i).AudioController;
                var sound = filter.Get1(i).SoundType;
                audioController.PlayAudioClip(sound);
            }
            
            _subsystems?.Run();
        }

        public void Destroy()
        {
            _subsystems?.Destroy();
        }
    }
}