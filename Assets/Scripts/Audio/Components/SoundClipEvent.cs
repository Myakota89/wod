﻿using Core;

namespace Audio.Components
{
    public struct SoundClipEvent
    {
        public SoundType SoundType;
    }
}