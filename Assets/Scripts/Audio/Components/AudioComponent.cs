﻿using System;

namespace Audio.Components
{
    [Serializable]
    public struct AudioComponent
    {
        public AudioController AudioController;
    }
}