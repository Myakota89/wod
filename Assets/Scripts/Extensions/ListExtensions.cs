﻿using System.Collections.Generic;
using Random = UnityEngine.Random;

namespace Extensions
{
    public static class ListExtensions
    {
        public static T GetRandom<T>(this List<T> source)
        {
            var rnd = Random.Range(0, source.Count);
            return source[rnd];
        }

        public static T GetRandomExclude<T>(this List<T> source, T exclude)
        {
            var rnd = GetRandom(source);

            while (rnd.Equals(exclude))
            {
                rnd = GetRandom(source);
            }

            return rnd;
        }
    }
}