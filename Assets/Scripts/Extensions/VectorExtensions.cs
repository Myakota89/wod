﻿using UnityEngine;

namespace Extensions
{
    public static class VectorExtensions
    {
        public static Vector2 ToVector2(this Vector2Int source)
        {
            return new Vector2(source.x, source.y);
        }
    }
}