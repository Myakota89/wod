﻿using System;
using Enum;

public struct GameProgress
{
    public int MaxProgressStep;
    public bool isBoardEnd => ProgressStep >= MaxProgressStep;
    public int Coins;
    public int ProgressStep;
    public GameState GameState;
}