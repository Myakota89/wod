﻿using UnityEngine;

namespace Spawn.Components
{
    public struct SpawnLampEvent
    {
        public Vector2Int SpawnPosition;
    }
}