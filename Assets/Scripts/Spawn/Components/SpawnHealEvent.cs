﻿using UnityEngine;

namespace Spawn.Components
{
    public struct SpawnHealEvent
    {
        public Vector2Int SpawnPosition;
    }
}