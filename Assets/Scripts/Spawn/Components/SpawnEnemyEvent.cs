﻿using UnityEngine;

namespace Spawn.Components
{
    public struct SpawnEnemyEvent
    {
        public Vector2Int SpawnPosition;
        public GameObject GameObject;
    }
}