﻿using UnityEngine;

namespace Spawn.Components
{
    public struct SpawnCellEvent
    {
        public Vector2Int SpawnPosition;
    }
}