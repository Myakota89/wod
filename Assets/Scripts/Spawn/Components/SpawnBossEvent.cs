﻿using UnityEngine;

namespace Spawn.Components
{
    public struct SpawnBossEvent
    {
        public Vector2Int SpawnPosition;
    }
}