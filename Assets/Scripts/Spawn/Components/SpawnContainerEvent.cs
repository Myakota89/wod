﻿using UnityEngine;

namespace Spawn.Components
{
    public struct SpawnContainerEvent
    {
        public Vector2Int SpawnPosition;
        public GameObject GameObject;
    }
}