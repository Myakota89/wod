﻿using UnityEngine;

namespace Spawn.Components
{
    public struct SpawnWorkshopEvent
    {
        public Vector2Int SpawnPosition;
    }
}