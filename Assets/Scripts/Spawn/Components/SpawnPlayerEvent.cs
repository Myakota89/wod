﻿using UnityEngine;

namespace Spawn.Components
{
    public struct SpawnPlayerEvent
    {
        public Vector2Int SpawnPosition;
    }
}