﻿using Leopotam.Ecs;
using Spawn.Components;
using Spawn.Subsystems;

namespace Spawn.Systems
{
    public class SpawnSystem : IEcsInitSystem, IEcsRunSystem, IEcsDestroySystem
    {
        private EcsWorld _world;
        private EcsSystems _subsystems;

        public void Init()
        {
            _subsystems = new EcsSystems(_world);
            _subsystems
                .Add(new SpawnPlayerSubsystem())
                .Add(new SpawnBossSubsystem())
                .Add(new SpawnEnemySubsystem())
                .Add(new SpawnHealSubsystem())
                .Add(new SpawnContainerSubsystem())
                .Add(new SpawnCellSubsystem())
                .Add(new SpawnLampSubsystem())
                .Add(new SpawnWorkshopSubsystem())
                .OneFrame<SpawnPlayerEvent>()
                .OneFrame<SpawnBossEvent>()
                .OneFrame<SpawnEnemyEvent>()
                .OneFrame<SpawnHealEvent>()
                .OneFrame<SpawnContainerEvent>()
                .OneFrame<SpawnCellEvent>()
                .OneFrame<SpawnLampEvent>()
                .OneFrame<SpawnWorkshopEvent>()
                .Init();
        }

        public void Run()
        {
            _subsystems?.Run();
        }

        public void Destroy()
        {
            _subsystems?.Destroy();
        }
    }
}