﻿using Board.Components;
using Common.Providers;
using Leopotam.Ecs;
using Spawn.Components;
using UnityEngine;

namespace Spawn.Subsystems
{
    public class SpawnCellSubsystem : IEcsRunSystem
    {
        private EcsFilter<SpawnCellEvent> _filter;
        private EcsFilter<BoardComponent> _boardEntity;
        
        public void Run()
        {
            foreach (var i in _filter)
            {
                var position = _filter.Get1(i).SpawnPosition;

                var go = Resources.Load<GameObject>("Prefabs/Cells/Cell");
                
                var unitData = go.GetComponent<UnitProvider>();
                unitData.SetPosition(position);
                
                var spawnPosition = new Vector2(position.x, -position.y);
                Object.Instantiate(go, spawnPosition, Quaternion.identity, _boardEntity.Get1(0).BoardParent);
            }
        }
    }
}