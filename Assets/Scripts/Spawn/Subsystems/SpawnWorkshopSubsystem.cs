﻿using Board.Components;
using Common.Providers;
using Leopotam.Ecs;
using Spawn.Components;
using UnityEngine;

namespace Spawn.Subsystems
{
    public class SpawnWorkshopSubsystem : IEcsRunSystem
    {
        private EcsFilter<SpawnWorkshopEvent> _filter;
        private EcsFilter<BoardComponent> _boardEntity;

        public void Run()
        {
            foreach (var i in _filter)
            {
                var spawnComponent = _filter.Get1(i);
                
                var prefab = Resources.Load<GameObject>("Prefabs/Workshop/Workshop 1");
                
                var unitData = prefab.GetComponent<UnitProvider>();
                unitData.SetPosition(spawnComponent.SpawnPosition);

                var spawnPosition = new Vector2(spawnComponent.SpawnPosition.x, -spawnComponent.SpawnPosition.y);
                Object.Instantiate(prefab, spawnPosition, Quaternion.identity, _boardEntity.Get1(0).BoardParent);
            }
        }
    }
}