﻿using Board.Components;
using Common.Providers;
using Leopotam.Ecs;
using Spawn.Components;
using UnityEngine;

namespace Spawn.Systems
{
    public class SpawnEnemySubsystem : IEcsRunSystem
    {
        private EcsFilter<SpawnEnemyEvent> _filter;
        private EcsFilter<BoardComponent> _boardEntity;

        public void Run()
        {
            foreach (var i in _filter)
            {
                var spawnComponent = _filter.Get1(i);
                
                var unitData = spawnComponent.GameObject.GetComponent<UnitProvider>();
                unitData.SetPosition(spawnComponent.SpawnPosition);
                
                var spawnPosition = new Vector2(spawnComponent.SpawnPosition.x, -spawnComponent.SpawnPosition.y);
                Object.Instantiate(spawnComponent.GameObject, spawnPosition, Quaternion.identity, _boardEntity.Get1(0).BoardParent);
            }
        }
    }
}