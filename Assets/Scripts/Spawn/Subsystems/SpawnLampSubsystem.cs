﻿using Animation.Spawn.Providers;
using Board.Components;
using Common.Providers;
using DesperateDevs.Utils;
using Input.Components;
using Leopotam.Ecs;
using Spawn.Components;
using Units.Collect.Lamp.Providers;
using UnityEngine;

namespace Spawn.Subsystems
{
    public class SpawnLampSubsystem : IEcsRunSystem
    {
        private EcsFilter<SpawnLampEvent> _filter;
        private EcsFilter<BoardComponent> _boardEntity;
        private EcsFilter<InputComponent> _inputEnity;

        public void Run()
        {
            foreach (var i in _filter)
            {
                var spawnComponent = _filter.Get1(i);
                
                var prefab = Resources.Load<GameObject>("Prefabs/Lamp/Lamp 1");
                
                var unitData = prefab.GetComponent<UnitProvider>();
                unitData.SetPosition(spawnComponent.SpawnPosition);

                var lampCount = Random.Range(0, 20);

                var lampData = prefab.GetComponent<LampProvider>();
                lampData.SetLampCount(lampCount);

                var spawnPosition = new Vector2(spawnComponent.SpawnPosition.x, -spawnComponent.SpawnPosition.y);
                Object.Instantiate(prefab, spawnPosition, Quaternion.identity, _boardEntity.Get1(0).BoardParent);
            }
        }
    }
}