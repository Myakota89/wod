﻿using Board.Components;
using Common.Providers;
using Equipments.Head;
using Equipments.Platform;
using Equipments.Weapon;
using Leopotam.Ecs;
using ScriptsSO;
using Spawn.Components;
using UnityEngine;

namespace Spawn.Systems
{
    public class SpawnPlayerSubsystem : IEcsRunSystem
    {
        private EcsFilter<SpawnPlayerEvent> filter;
        private EcsFilter<BoardComponent> boardEntity;

        public void Run()
        {
            foreach (var i in filter)
            {
                var spawnComponent = filter.Get1(i);
                
                var prefab = Resources.Load<GameObject>("Prefabs/Player/Player");
                
                var weapon = prefab.GetComponent<WeaponProvider>();
                weapon.Value = ResourceManager.Equipments.StartWeaponData.GetWeaponComponent();

                var head = prefab.GetComponent<HeadProvider>();
                head.Value = ResourceManager.Equipments.StartHeadData.GetHeadComponent();
                
                var platform = prefab.GetComponent<PlatformProvider>();
                platform.Value = ResourceManager.Equipments.StartPlatformData.GetPlatformComponent();

                var unitData = prefab.GetComponent<UnitProvider>();
                unitData.SetPosition(spawnComponent.SpawnPosition);

                var spawnPosition = new Vector2(spawnComponent.SpawnPosition.x, -spawnComponent.SpawnPosition.y);
                Object.Instantiate(prefab, spawnPosition, Quaternion.identity, boardEntity.Get1(0).BoardParent);
            }
        }
    }
}