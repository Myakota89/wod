﻿using Board.Components;
using Common.Providers;
using Leopotam.Ecs;
using Spawn.Components;
using Units.Collect.Healer.Providers;
using UnityEngine;

namespace Spawn.Systems
{
    public class SpawnHealSubsystem : IEcsRunSystem
    {
        private EcsFilter<SpawnHealEvent> filter;
        private EcsFilter<BoardComponent> boardEntity;

        public void Run()
        {
            foreach (var i in filter)
            {
                var spawnComponent = filter.Get1(i);

                GameObject prefab;

                if (Random.value > 0.5f)
                    prefab = GetArmorHealPrefab();
                else
                    prefab = GetHealthHealPrefab();
                
                var unitData = prefab.GetComponent<UnitProvider>();
                unitData.SetPosition(spawnComponent.SpawnPosition);
                
                var spawnPosition = new Vector2(spawnComponent.SpawnPosition.x, -spawnComponent.SpawnPosition.y);
                Object.Instantiate(prefab, spawnPosition, Quaternion.identity, boardEntity.Get1(0).BoardParent);
            }
        }

        private GameObject GetArmorHealPrefab()
        {
            var prefab = Resources.Load<GameObject>("Prefabs/Heal/ArmorHeal 1");

            var heal = prefab.GetComponent<HealProvider>();
            heal.SetArmorValue(Random.Range(1, 11));

            return prefab;
        }
        
        private GameObject GetHealthHealPrefab()
        {
            var prefab = Resources.Load<GameObject>("Prefabs/Heal/HealthHeal 1");

            var heal = prefab.GetComponent<HealProvider>();
            heal.SetHealthValue(Random.Range(1, 11));

            return prefab;
        }
    }
}