﻿using Enum;
using GameCamera.Components;
using Leopotam.Ecs;
using Move.Components;

namespace State
{
    public class StateSystem : IEcsRunSystem
    {
        private EcsFilter<MovableEvent> _movableFilter;
        private EcsFilter<CameraMoveEvent> _cameraFilter;

        public void Run()
        {
            if (!_movableFilter.IsEmpty())
            {
                Game.TurnState = TurnState.PlayerMove;
            }
            else if (!_cameraFilter.IsEmpty())
            {
                Game.TurnState = TurnState.CameraMoving;
            }
            else
            {
                Game.TurnState = TurnState.AwaitInput;
            }
        }
    }
}