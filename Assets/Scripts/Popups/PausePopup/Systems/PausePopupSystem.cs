﻿using Audio;
using Core;
using Leopotam.Ecs;
using Leopotam.Ecs.Ui.Components;
using Leopotam.Ecs.Ui.Systems;
using Popups.Common.ClosePopup;
using Popups.PausePopup.Components;
using UnityEngine.SceneManagement;

namespace Popups.PausePopup.Systems
{
    public class PausePopupSystem : IEcsRunSystem
    {
        private EcsFilter<PausePopupComponent> _pauseEntity;
        private EcsFilter<EcsUiClickEvent> _filter;
        private EcsUiEmitter _canvas;

        public void Run()
        {
            foreach (var i in _filter)
            {
                switch (_filter.Get1(i).WidgetName)
                {
                    case "PauseButton" :
                        PopupManager.OpenPopup<Providers.PausePopup>();
                        AudioManager.PlayAudio(SoundType.ClickButton);
                        break;
                    case "PausePopupContinue" :
                        _pauseEntity.GetEntity(0).Get<ClosePopupEvent>();
                        AudioManager.PlayAudio(SoundType.ClickButton);
                        break;
                    case "PausePopupToMainMenu":
                    case "PausePopupRestart" :
                        SceneManager.LoadScene("Game");
                        break;
                }
            }
        }
    }
}