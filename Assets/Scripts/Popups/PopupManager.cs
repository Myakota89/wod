﻿using Leopotam.Ecs.Ui.Systems;
using UnityEngine;
using Voody.UniLeo;

namespace Popups
{
    public static class PopupManager
    {
        private const string PopupsPath = "Popups";
        private static Transform _popupParent;
        
        public static void SetPopupParent()
        {
            _popupParent = Object.FindObjectOfType<EcsUiEmitter>().transform;
        }

        public static T OpenPopup<T>() where T : BaseMonoProvider
        {
            var prefab = Resources.Load<T>($"{PopupsPath}/{typeof(T).Name}");
            var popup = Object.Instantiate(prefab, _popupParent);

            return popup;
        }
    }
}