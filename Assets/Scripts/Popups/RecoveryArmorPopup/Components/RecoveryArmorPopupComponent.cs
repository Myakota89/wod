﻿using System;
using TMPro;
using UnityEngine.UI;

namespace Popups.RecoveryArmorPopup.Components
{
    [Serializable]
    public struct RecoveryArmorPopupComponent
    {
        public TextMeshProUGUI Title;
        public TextMeshProUGUI HeadArmor;
        public TextMeshProUGUI PlatformArmor;
        public TextMeshProUGUI WeaponArmor;
        public Image HeadSprite;
        public Image PlatformSprite;
        public Image LeftWeaponSprite;
        public Image RightWeaponSprite;
        public int ArmorHeal;
    }
}