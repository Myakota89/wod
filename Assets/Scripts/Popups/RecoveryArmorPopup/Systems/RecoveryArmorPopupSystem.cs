﻿using Audio;
using Core;
using Leopotam.Ecs;
using Leopotam.Ecs.Ui.Components;
using Player;
using Popups.Common.ClosePopup;
using Popups.RecoveryArmorPopup.Components;
using Recovery.Events;

namespace Popups.RecoveryArmorPopup.Systems
{
    public class RecoveryArmorPopupSystem : IEcsRunSystem
    {
        private EcsFilter<RecoveryArmorPopupComponent> _filter;
        private EcsFilter<PlayerTag> _playerEntity;
        private EcsFilter<EcsUiClickEvent> _clickEvent;

        public void Run()
        {
            foreach (var i in _clickEvent)
            {
                ref var player = ref _playerEntity.GetEntity(0);
                ref var healValue = ref _filter.Get1(0).ArmorHeal;
                
                switch (_clickEvent.Get1(i).WidgetName)
                {
                    case "HealHeadArmorButton":
                        player.Get<HeadRecoveryEvent>().Armor = healValue;
                        _filter.GetEntity(i).Get<ClosePopupEvent>();
                        AudioManager.PlayAudio(SoundType.ClickButton);
                        break;
                    case "HealPlatformArmorButton":
                        player.Get<PlatformRecoveryEvent>().Armor = healValue;
                        _filter.GetEntity(i).Get<ClosePopupEvent>();
                        AudioManager.PlayAudio(SoundType.ClickButton);
                        break;
                    case "HealWeaponArmorButton":
                        player.Get<WeaponRecoveryEvent>().Armor = healValue;
                        _filter.GetEntity(i).Get<ClosePopupEvent>();
                        AudioManager.PlayAudio(SoundType.ClickButton);
                        break;
                }

                
            }
        }
    }
}