﻿using Equipments.Head;
using Equipments.Platform;
using Equipments.Weapon;
using Popups.RecoveryArmorPopup.Components;
using Voody.UniLeo;

namespace Popups.RecoveryArmorPopup.Providers
{
    public class RecoveryArmorPopup : MonoProvider<RecoveryArmorPopupComponent>
    {
        public void SetArmorValue(int heal)
        {
            value.ArmorHeal = heal;
        }
        
        public void SetTitle(string text)
        {
            value.Title.text = text;
        }

        public void SetHeadData(HeadComponent head)
        {
            value.HeadArmor.text = head.CurrentArmor.ToString();
            value.HeadSprite.sprite = head.Sprite;
        }
        
        public void SetPlatformData(PlatformComponent platform)
        {
            value.PlatformArmor.text = platform.CurrentArmor.ToString();
            value.PlatformSprite.sprite = platform.Sprite;
        }

        public void SetWeaponData(WeaponComponent weapon)
        {
            value.WeaponArmor.text = weapon.CurrentArmor.ToString();
            value.LeftWeaponSprite.sprite = weapon.LeftWeaponSprite;
            value.RightWeaponSprite.sprite = weapon.RightWeaponSprite;
        }
    }
}