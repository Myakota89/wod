﻿using Audio;
using Core;
using Leopotam.Ecs;
using Leopotam.Ecs.Ui.Components;
using Popups.ChangeEquipmentPopup.Components;
using Popups.ChangeEquipmentPopup.Subsystems;
using Popups.Common.ClosePopup;
using UnityEngine;

namespace Popups.ChangeEquipmentPopup.Systems
{
    public class ChangeEquipmentPopupSystem : IEcsInitSystem, IEcsRunSystem, IEcsDestroySystem
    {
        private EcsFilter<ChangeEquipmentPopupComponent> _filter;
        private EcsFilter<EcsUiClickEvent> _clickFilter;

        private EcsWorld _world;
        private EcsSystems _popupSubsystems;
        
        public void Init()
        {
            _popupSubsystems = new EcsSystems(_world);
            _popupSubsystems
                .Add(new ChangeHeadSubsystem())
                .Add(new ChangePlatformSubsystem())
                .Add(new ChangeWeaponSubsystem())
                .OneFrame<OfferButtonClick>()
                .Init();

        }

        public void Run()
        {
            foreach (var i in _filter)
            {
                if (!_filter.Get1(i).IsInitialized)
                {
                    SetPopupEquipments(ref _filter.GetEntity(i));
                }
            }
            
            foreach (var i in _clickFilter)
            {
                ref var click = ref _clickFilter.Get1(i);

                switch (click.WidgetName)
                {
                    case "PlayerButton":
                        _filter.GetEntity(0).Get<ClosePopupEvent>();
                        AudioManager.PlayAudio(SoundType.ClickButton);
                        break;
                    
                    case "OfferButton":
                        _filter.GetEntity(0).Get<OfferButtonClick>();
                        _filter.GetEntity(0).Get<ClosePopupEvent>();
                        AudioManager.PlayAudio(SoundType.ClickButton);
                        break;
                }
            }
            
            _popupSubsystems?.Run();
        }

        private void SetPopupEquipments(ref EcsEntity entity)
        {
            var rnd = Random.value;
            
            if (rnd > 0.7f)
                entity.Get<ChangeWeaponSubcomponent>();
            else if (rnd > 0.3f)
                entity.Get<ChangeHeadSubcomponent>();
            else
                entity.Get<ChangePlatformSubcomponent>();
        }

        public void Destroy()
        {
            _popupSubsystems?.Destroy();
        }
    }
}