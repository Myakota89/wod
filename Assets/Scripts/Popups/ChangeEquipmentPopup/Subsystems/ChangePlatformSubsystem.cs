﻿using Equipments.Platform;
using Leopotam.Ecs;
using Player;
using Popups.ChangeEquipmentPopup.Components;
using ScriptsSO;

namespace Popups.ChangeEquipmentPopup.Subsystems
{
    public class ChangePlatformSubsystem : IEcsRunSystem
    {
        private EcsFilter<ChangeEquipmentPopupComponent, ChangePlatformSubcomponent> _filter;
        private EcsFilter<PlayerTag, PlatformComponent> _playerFilter;

        public void Run()
        {
            foreach (var i in _filter)
            {
                ref var popup = ref _filter.Get1(i);
                
                if (!popup.IsInitialized)
                {
                    var platformType = _playerFilter.Get2(i).PlatformType;
                    _filter.Get2(i).OfferPlatform = ResourceManager.Equipments.GetPlatformDataExclude(platformType).GetPlatformComponent();
                    
                    popup.PlayerButton.SetPlatform(_playerFilter.Get2(i));
                    popup.OfferButton.SetPlatform(_filter.Get2(i).OfferPlatform);
                    
                    popup.IsInitialized = true;
                }
                
                if (_filter.GetEntity(i).Has<OfferButtonClick>())
                {
                    _playerFilter.GetEntity(0).Replace(_filter.Get2(i).OfferPlatform);
                    _filter.GetEntity(i).Del<OfferButtonClick>();
                }
            }
        }
    }
}