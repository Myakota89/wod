﻿using Equipments.Head;
using Leopotam.Ecs;
using Player;
using Popups.ChangeEquipmentPopup.Components;
using ScriptsSO;

namespace Popups.ChangeEquipmentPopup.Subsystems
{
    public class ChangeHeadSubsystem : IEcsRunSystem
    {
        private EcsFilter<ChangeEquipmentPopupComponent, ChangeHeadSubcomponent> _filter;
        private EcsFilter<PlayerTag, HeadComponent> _playerFilter;

        public void Run()
        {
            foreach (var i in _filter)
            {
                ref var popup = ref _filter.Get1(i);
                
                if (!popup.IsInitialized)
                {
                    var headType = _playerFilter.Get2(i).HeadType;
                    _filter.Get2(i).OfferHead = ResourceManager.Equipments.GetHeadDataExclude(headType).GetHeadComponent();
                    
                    popup.PlayerButton.SetHead(_playerFilter.Get2(i));
                    popup.OfferButton.SetHead(_filter.Get2(i).OfferHead);
                    
                    popup.IsInitialized = true;
                }
                
                if (_filter.GetEntity(i).Has<OfferButtonClick>())
                {
                    _playerFilter.GetEntity(0).Replace(_filter.Get2(i).OfferHead);
                    _filter.GetEntity(i).Del<OfferButtonClick>();
                }
            }
        }
    }
}