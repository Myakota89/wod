﻿using Equipments.Weapon;
using Leopotam.Ecs;
using Player;
using Popups.ChangeEquipmentPopup.Components;
using ScriptsSO;

namespace Popups.ChangeEquipmentPopup.Subsystems
{
    public class ChangeWeaponSubsystem : IEcsRunSystem
    {
        private EcsFilter<ChangeEquipmentPopupComponent, ChangeWeaponSubcomponent> _filter;
        private EcsFilter<PlayerTag, WeaponComponent> _playerFilter;

        public void Run()
        {
            foreach (var i in _filter)
            {
                ref var popup = ref _filter.Get1(i);
                
                if (!popup.IsInitialized)
                {
                    var weaponType = _playerFilter.Get2(i).WeaponType;
                    _filter.Get2(i).OfferWeapon = ResourceManager.Equipments.GetWeaponDataExclude(weaponType).GetWeaponComponent();
                    
                    popup.PlayerButton.SetWeapon(_playerFilter.Get2(i));
                    popup.OfferButton.SetWeapon(_filter.Get2(i).OfferWeapon);
                    
                    popup.IsInitialized = true;
                }
                
                if (_filter.GetEntity(i).Has<OfferButtonClick>())
                {
                    _playerFilter.GetEntity(0).Replace(_filter.Get2(i).OfferWeapon);
                    _filter.GetEntity(i).Del<OfferButtonClick>();
                }
            }
        }
    }
}