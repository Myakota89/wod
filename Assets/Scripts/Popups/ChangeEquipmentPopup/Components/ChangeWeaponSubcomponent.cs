﻿using Equipments.Weapon;

namespace Popups.ChangeEquipmentPopup.Components
{
    public struct ChangeWeaponSubcomponent
    {
        public WeaponComponent OfferWeapon;
    }
}