﻿using Equipments.Head;

namespace Popups.ChangeEquipmentPopup.Components
{
    public struct ChangeHeadSubcomponent
    {
        public HeadComponent OfferHead;
    }
}