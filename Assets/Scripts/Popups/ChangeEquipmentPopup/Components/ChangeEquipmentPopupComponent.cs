﻿using System;
using Popups.ChangeEquipmentPopup.View;

namespace Popups.ChangeEquipmentPopup.Components
{
    [Serializable]
    public struct ChangeEquipmentPopupComponent
    {
        public EquipmentPopupButton PlayerButton;
        public EquipmentPopupButton OfferButton;
        public bool IsInitialized;
    }
}