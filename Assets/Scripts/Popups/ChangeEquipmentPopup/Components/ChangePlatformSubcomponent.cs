﻿using Equipments.Platform;

namespace Popups.ChangeEquipmentPopup.Components
{
    public struct ChangePlatformSubcomponent
    {
        public PlatformComponent OfferPlatform;
    }
}