﻿using Equipments.Head;
using Equipments.Platform;
using Equipments.Weapon;
using UnityEngine;
using UnityEngine.UI;

namespace Popups.ChangeEquipmentPopup.View
{
    public class EquipmentPopupButton : MonoBehaviour
    {
        public Image HeadSR;
        public Image PlatformSR;
        public Image WeaponLeftSR;
        public Image WeaponRightSR;
        
        public void SetHead(HeadComponent component)
        {
            DeactivateAllSprites();
            HeadSR.sprite = component.Sprite;
            HeadSR.gameObject.SetActive(true);
        }

        public void SetPlatform(PlatformComponent component)
        {
            DeactivateAllSprites();
            PlatformSR.sprite = component.Sprite;
            PlatformSR.gameObject.SetActive(true);
        }

        public void SetWeapon(WeaponComponent component)
        {
            DeactivateAllSprites();
            WeaponLeftSR.sprite = component.LeftWeaponSprite;
            WeaponRightSR.sprite = component.RightWeaponSprite;
            WeaponLeftSR.gameObject.SetActive(true);
            WeaponRightSR.gameObject.SetActive(true);
        }

        private void DeactivateAllSprites()
        {
            HeadSR.gameObject.SetActive(false);
            PlatformSR.gameObject.SetActive(false);
            WeaponLeftSR.gameObject.SetActive(false);
            WeaponRightSR.gameObject.SetActive(false);
        }
    }
}