﻿using Leopotam.Ecs;
using Leopotam.Ecs.Ui.Components;
using Leopotam.Ecs.Ui.Systems;
using Popups.ChangeEquipmentPopup.Systems;
using Popups.Common.ClosePopup;
using Popups.Common.ShowPopup;
using Popups.LosePopup.Systems;
using Popups.PausePopup.Systems;
using Popups.RecoveryArmorPopup.Systems;
using Popups.RecoveryHealthPopup.Systems;
using Popups.WinPopup.Systems;

namespace Popups
{
    public class PopupSystem : IEcsInitSystem, IEcsRunSystem, IEcsDestroySystem
    {
        private EcsWorld _world;
        private EcsSystems popupSystems;
        private EcsUiEmitter _canvas;

        public void Init()
        {
            popupSystems = new EcsSystems(_world);
            popupSystems
                .Add(new StandardShowPopupSystem())
                .Add(new StandardClosePopupSystem())
                .Add(new ChangeEquipmentPopupSystem())
                .Add(new PausePopupSystem())
                .Add(new WinPopupSystem())
                .Add(new LosePopupSystem())
                .Add(new RecoveryHealthPopupSystem())
                .Add(new RecoveryArmorPopupSystem())
                .OneFrame<EcsUiClickEvent>()
                .Inject(_canvas)
                .Init();
        }

        public void Run()
        {
            popupSystems?.Run();
        }

        public void Destroy()
        {
            popupSystems?.Destroy();
        }
    }
}