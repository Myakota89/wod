﻿using System;
using TMPro;
using UnityEngine.UI;

namespace Popups.RecoveryHealthPopup.Components
{
    [Serializable]
    public struct RecoveryHealthPopupComponent
    {
        public TextMeshProUGUI Title;
        public TextMeshProUGUI HeadHealth;
        public TextMeshProUGUI PlatformHealth;
        public TextMeshProUGUI WeaponHealth;
        public Image HeadSprite;
        public Image PlatformSprite;
        public Image LeftWeaponSprite;
        public Image RightWeaponSprite;
        public int HealthHeal;
    }
}