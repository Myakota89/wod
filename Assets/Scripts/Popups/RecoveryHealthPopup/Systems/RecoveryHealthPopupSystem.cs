﻿using Audio;
using Core;
using Leopotam.Ecs;
using Leopotam.Ecs.Ui.Components;
using Player;
using Popups.Common.ClosePopup;
using Popups.RecoveryHealthPopup.Components;
using Recovery.Events;

namespace Popups.RecoveryHealthPopup.Systems
{
    public class RecoveryHealthPopupSystem : IEcsRunSystem
    {
        private EcsFilter<RecoveryHealthPopupComponent> _filter;
        private EcsFilter<PlayerTag> _playerEntity;
        private EcsFilter<EcsUiClickEvent> _clickEvent;

        public void Run()
        {
            foreach (var i in _clickEvent)
            {
                ref var player = ref _playerEntity.GetEntity(0);
                ref var healValue = ref _filter.Get1(0).HealthHeal;
                
                switch (_clickEvent.Get1(i).WidgetName)
                {
                    case "HealHeadHealthButton":
                        player.Get<HeadRecoveryEvent>().Health = healValue;
                        _filter.GetEntity(0).Get<ClosePopupEvent>();
                        AudioManager.PlayAudio(SoundType.ClickButton);
                        break;
                    case "HealPlatformHealthButton":
                        player.Get<PlatformRecoveryEvent>().Health = healValue;
                        _filter.GetEntity(0).Get<ClosePopupEvent>();
                        AudioManager.PlayAudio(SoundType.ClickButton);
                        break;
                    case "HealWeaponHealthButton":
                        player.Get<WeaponRecoveryEvent>().Health = healValue;
                        _filter.GetEntity(0).Get<ClosePopupEvent>();
                        AudioManager.PlayAudio(SoundType.ClickButton);
                        break;
                }

                
            }
        }
    }
}