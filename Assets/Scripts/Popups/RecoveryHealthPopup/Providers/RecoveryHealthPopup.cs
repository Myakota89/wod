﻿using Equipments.Head;
using Equipments.Platform;
using Equipments.Weapon;
using Popups.RecoveryHealthPopup.Components;
using Voody.UniLeo;

namespace Popups.RecoveryHealthPopup.Providers
{
    public class RecoveryHealthPopup : MonoProvider<RecoveryHealthPopupComponent>
    {
        public void SetHealthValue(int heal)
        {
            value.HealthHeal = heal;
        }
        
        public void SetTitle(string text)
        {
            value.Title.text = text;
        }

        public void SetHeadData(HeadComponent head)
        {
            value.HeadHealth.text = head.CurrentHealth.ToString();
            value.HeadSprite.sprite = head.Sprite;
        }
        
        public void SetPlatformData(PlatformComponent platform)
        {
            value.PlatformHealth.text = platform.CurrentHealth.ToString();
            value.PlatformSprite.sprite = platform.Sprite;
        }

        public void SetWeaponData(WeaponComponent weapon)
        {
            value.WeaponHealth.text = weapon.CurrentHealth.ToString();
            value.LeftWeaponSprite.sprite = weapon.LeftWeaponSprite;
            value.RightWeaponSprite.sprite = weapon.RightWeaponSprite;
        }
    }
}