﻿using Leopotam.Ecs;
using Leopotam.Ecs.Ui.Components;
using Leopotam.Ecs.Ui.Systems;
using UnityEngine.SceneManagement;

namespace Popups.LosePopup.Systems
{
    public class LosePopupSystem : IEcsRunSystem
    {
        private EcsFilter<EcsUiClickEvent> _filter;
        private EcsUiEmitter _canvas;

        public void Run()
        {
            foreach (var i in _filter)
            {
                switch (_filter.Get1(i).WidgetName)
                {
                    case "LosePopupToMainMenu":
                    case "LosePopupRestart" :
                        SceneManager.LoadScene("Game");
                        break;
                    
                }
            }
        }
    }
}