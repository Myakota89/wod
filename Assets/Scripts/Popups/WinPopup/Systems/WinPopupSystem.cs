﻿using Leopotam.Ecs;
using Leopotam.Ecs.Ui.Components;
using UnityEngine.SceneManagement;

namespace Popups.WinPopup.Systems
{
    public class WinPopupSystem : IEcsRunSystem
    {
        private EcsFilter<EcsUiClickEvent> _filter;

        public void Run()
        {
            foreach (var i in _filter)
            {
                switch (_filter.Get1(i).WidgetName)
                {
                    case "WinPopupToMainMenu":
                    case "WinPopupRestart" :
                        SceneManager.LoadScene("Game");
                        break;
                    
                }
            }
        }
    }
}