﻿using System.Linq;
using DG.Tweening;
using Leopotam.Ecs;
using Leopotam.Ecs.Ui.Actions;
using UnityEngine;
using UnityEngine.UI;

namespace Popups.Common.ClosePopup
{
    public class StandardClosePopupSystem : IEcsRunSystem
    {
        private EcsFilter<StandardClosePopupComponent, ClosePopupEvent> _filter;

        public void Run()
        {
            foreach (var i in _filter)
            {
                Close(_filter.Get1(i));
                _filter.GetEntity(i).Destroy();
            }
        }

        public void Close(StandardClosePopupComponent component)
        {
            var sequence = DOTween.Sequence();

            sequence.Append(GetIncreaseScaleTween(component))
                .Append(GetDecreaseScaleTween(component)).
                OnComplete(() =>
                {
                    Object.Destroy(component.Transform.gameObject);
                });

            var buttons = component.Transform.gameObject.GetComponentsInChildren<EcsUiClickAction>().ToList();
            buttons.ForEach(b => b.enabled = false);
        }

        private Tween GetIncreaseScaleTween(StandardClosePopupComponent component)
        {
            return component.Transform.DOScale(component.StartShowScale, component.Duration)
                .SetEase(Ease.Linear);
        }
        
        private Tween GetDecreaseScaleTween(StandardClosePopupComponent component)
        {
            return component.Transform.DOScale(component.EndShowScale, component.Duration)
                .SetEase(Ease.Linear);
        }
    }
}