﻿using System;
using UnityEngine;

namespace Popups.Common.ClosePopup
{
    [Serializable]
    public struct StandardClosePopupComponent
    {
        public float StartShowScale;
        public float EndShowScale;
        public float Duration;
        public Transform Transform;
    }
}