﻿using System;
using UnityEngine;

namespace Popups.Common.ShowPopup
{
    [Serializable]
    public struct StandardShowPopupComponent
    {
        public float StartShowScale;
        public float EndShowScale;
        public float Duration;
        public Transform Transform;
    }
}