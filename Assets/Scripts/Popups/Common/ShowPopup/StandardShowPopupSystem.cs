﻿using DG.Tweening;
using Leopotam.Ecs;
using UnityEngine;

namespace Popups.Common.ShowPopup
{
    public class StandardShowPopupSystem : IEcsRunSystem
    {
        private EcsFilter<StandardShowPopupComponent, ShowPopupEvent> _filter;

        public void Run()
        {
            foreach (var i in _filter)
            {
                Show(_filter.Get1(i));
                _filter.GetEntity(i).Del<ShowPopupEvent>();
            }
        }
        
        private void Show(StandardShowPopupComponent component)
        {
            var sequence = DOTween.Sequence();

            sequence.Append(GetIncreaseScaleTween(component))
                .Append(GetDecreaseScaleTween(component));
        }

        private Tween GetIncreaseScaleTween(StandardShowPopupComponent component)
        {
            component.Transform.localScale = Vector3.one * component.StartShowScale;

            return component.Transform.DOScale(component.EndShowScale, component.Duration)
                .SetEase(Ease.Linear);
        }
        
        private Tween GetDecreaseScaleTween(StandardShowPopupComponent component)
        {
            return component.Transform.DOScale(1, component.Duration)
                .SetEase(Ease.Linear);
        }
    }
}