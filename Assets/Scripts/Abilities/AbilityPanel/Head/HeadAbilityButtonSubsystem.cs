﻿using Abilities.AbilityPanel.Common.Components;
using Abilities.AbilityPanel.Common.Events;
using Leopotam.Ecs;
using Leopotam.Ecs.Ui.Components;

namespace Abilities.AbilityPanel.Head
{
    public class HeadAbilityButtonSubsystem : IEcsRunSystem
    {
        private EcsFilter<HeadAbilityButtonTag, AbilityButtonComponent> _headButtonEntity;
        private EcsFilter<EcsUiClickEvent> _clickEvent;

        public void Run()
        {
            foreach (var i in _clickEvent)
            {
                if (_clickEvent.Get1(i).WidgetName == "HeadAbilityButton")
                {
                    _headButtonEntity.GetEntity(0).Get<AbilityButtonClickEvent>();
                }
            }
        }
    }
}