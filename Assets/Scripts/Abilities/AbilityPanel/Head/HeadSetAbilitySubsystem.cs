﻿using Abilities.AbilityPanel.Common.Components;
using Equipments.Head;
using Leopotam.Ecs;
using Player;

namespace Abilities.AbilityPanel.Head
{
    public class HeadSetAbilitySubsystem : IEcsRunSystem
    {
        private EcsFilter<PlayerTag, HeadComponent> _playerEntity;
        private EcsFilter<HeadAbilityButtonTag, AbilityButtonComponent> _headButton;

        public void Run()
        {
            var ability = _playerEntity.Get2(0).ActiveAbility;

            if (ability != null)
                _headButton.Get2(0).Button.SetAbility(ability);
        }
    }
}