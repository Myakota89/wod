﻿using Abilities.AbilityPanel.Common.Components;
using Abilities.AbilityPanel.Common.Events;
using Input.Components;
using Leopotam.Ecs;
using ScriptsSO.Ability.Active;

namespace Abilities.AbilityPanel.Common.Subsystems
{
    public class SelectAbilityButtonSubsystem : IEcsRunSystem
    {
        private EcsFilter<AbilityButtonComponent, AbilityButtonClickEvent> _filter;
        private EcsFilter<InputComponent> _inputEntity;


        public void Run()
        {
            foreach (var i in _filter)
            {
                ref var selectedAbility = ref _inputEntity.Get1(0).SelectedAbility;
                ref var button = ref _filter.Get1(i).Button;
                var ability = button.Ability;
                
                if (ability != null && !ability.IsReady())
                    continue;

                switch (ability.Type)
                {
                    case TargetType.Target:
                        selectedAbility = selectedAbility == ability ? null : ability;
                        break;
                    
                    case TargetType.NonTarget:
                        ability.SetComponent(_inputEntity.GetEntity(0));
                        break;
                }
            }
        }
    }
}