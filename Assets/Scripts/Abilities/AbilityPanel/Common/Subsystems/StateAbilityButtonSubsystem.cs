﻿using Abilities.AbilityPanel.Common.Components;
using ButtonView;
using Input.Components;
using Leopotam.Ecs;

namespace Abilities.AbilityPanel.Common.Subsystems
{
    public class StateAbilityButtonSubsystem : IEcsRunSystem
    {
        private EcsFilter<AbilityButtonComponent> _filter;
        private EcsFilter<InputComponent> _inputEntity;

        public void Run()
        {
            foreach (var i in _filter)
            {
                ref var button = ref _filter.Get1(i).Button;
                var ability = button.Ability;

                if (ability == null)
                    continue;

                ref var selectedAbility = ref _inputEntity.Get1(0).SelectedAbility;

                if (!ability.IsReady())
                    button.SetButtonState(ButtonState.Reload);
                else if (selectedAbility == ability && ability != null)
                    button.SetButtonState(ButtonState.Select);
                else
                    button.SetButtonState(ButtonState.Deselect);
                
                button.SetCooldown(ability.CurrentCooldown);
            }
        }
    }
}