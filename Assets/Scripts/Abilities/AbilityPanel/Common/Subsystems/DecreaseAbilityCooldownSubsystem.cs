﻿using Abilities.AbilityPanel.Common.Components;
using Abilities.AbilityPanel.Common.Events;
using Leopotam.Ecs;

namespace Abilities.AbilityPanel.Common.Subsystems
{
    public class DecreaseAbilityCooldownSubsystem : IEcsRunSystem
    {
        private EcsFilter<DecreaseAbilityCooldownEvent> _decreaseEvent;
        private EcsFilter<AbilityButtonComponent> _filter;

        public void Run()
        {
            if (!_decreaseEvent.IsEmpty())
            {
                foreach (var i in _filter)
                {
                    var ability = _filter.Get1(i).Button.Ability;

                    if (ability != null)
                        ability.DecreaseCooldown(_decreaseEvent.Get1(0).Value);
                }
            }
        }
    }
}