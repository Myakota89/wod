﻿using Abilities.AbilityPanel.Common.Events;
using Abilities.AbilityPanel.Common.Subsystems;
using Abilities.AbilityPanel.Head;
using Abilities.AbilityPanel.Platform;
using Abilities.AbilityPanel.Weapon;
using Leopotam.Ecs;

namespace Abilities.AbilityPanel.Common.Systems
{
    public class AbilityPanelSystem : IEcsInitSystem, IEcsRunSystem, IEcsDestroySystem
    {
        private EcsWorld _world;
        private EcsSystems _subsystems;

        public void Init()
        {
            _subsystems = new EcsSystems(_world);
            _subsystems
                .Add(new WeaponSetAbilitySubsystem())
                .Add(new PlatformSetAbilitySubsystem())
                .Add(new HeadSetAbilitySubsystem())
                .Add(new StateAbilityButtonSubsystem())
                .Add(new WeaponAbilityButtonSubsystem())
                .Add(new PlatformAbilityButtonSubsystem())
                .Add(new HeadAbilityButtonSubsystem())
                .Add(new SelectAbilityButtonSubsystem())
                .Add(new DecreaseAbilityCooldownSubsystem())
                .OneFrame<DecreaseAbilityCooldownEvent>()
                .OneFrame<AbilityButtonClickEvent>()
                .Init();
        }

        public void Run()
        {
            _subsystems?.Run();
        }

        public void Destroy()
        {
            _subsystems?.Destroy();
        }
    }
}