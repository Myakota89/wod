﻿namespace Abilities.AbilityPanel.Common.Events
{
    public struct DecreaseAbilityCooldownEvent
    {
        public int Value;
    }
}