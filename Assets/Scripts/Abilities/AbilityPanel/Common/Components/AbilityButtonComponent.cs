﻿using System;
using ButtonView;

namespace Abilities.AbilityPanel.Common.Components
{
    [Serializable]
    public struct AbilityButtonComponent
    {
        public AbilityButton Button;
    }
}