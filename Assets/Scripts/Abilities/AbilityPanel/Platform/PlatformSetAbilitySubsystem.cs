﻿using Abilities.AbilityPanel.Common.Components;
using Equipments.Platform;
using Leopotam.Ecs;
using Player;

namespace Abilities.AbilityPanel.Platform
{
    public class PlatformSetAbilitySubsystem : IEcsRunSystem
    {
        private EcsFilter<PlayerTag, PlatformComponent> _playerEntity;
        private EcsFilter<PlatformAbilityButtonTag, AbilityButtonComponent> _platformButton;

        public void Run()
        {
            var ability = _playerEntity.Get2(0).ActiveAbility;

            if (ability != null)
                _platformButton.Get2(0).Button.SetAbility(ability);
        }
    }
}