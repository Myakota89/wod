﻿using Abilities.AbilityPanel.Common.Components;
using Abilities.AbilityPanel.Common.Events;
using Leopotam.Ecs;
using Leopotam.Ecs.Ui.Components;

namespace Abilities.AbilityPanel.Platform
{
    public class PlatformAbilityButtonSubsystem : IEcsRunSystem
    {
        private EcsFilter<PlatformAbilityButtonTag, AbilityButtonComponent> _platformButtonEntity;
        private EcsFilter<EcsUiClickEvent> _clickEvent;

        public void Run()
        {
            foreach (var i in _clickEvent)
            {
                if (_clickEvent.Get1(i).WidgetName == "PlatformAbilityButton")
                {
                    _platformButtonEntity.GetEntity(0).Get<AbilityButtonClickEvent>();
                }
            }
        }
    }
}