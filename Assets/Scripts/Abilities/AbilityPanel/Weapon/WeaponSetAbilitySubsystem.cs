﻿using Abilities.AbilityPanel.Common.Components;
using Equipments.Weapon;
using Leopotam.Ecs;
using Player;

namespace Abilities.AbilityPanel.Weapon
{
    public class WeaponSetAbilitySubsystem : IEcsRunSystem
    {
        private EcsFilter<PlayerTag, WeaponComponent> _playerEntity;
        private EcsFilter<WeaponAbilityButtonTag, AbilityButtonComponent> _weaponButton;

        public void Run()
        {
            var ability = _playerEntity.Get2(0).ActiveAbility;

            if (ability != null)
                _weaponButton.Get2(0).Button.SetAbility(ability);
        }
    }
}