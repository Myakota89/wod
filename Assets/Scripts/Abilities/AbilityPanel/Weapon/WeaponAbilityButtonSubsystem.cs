﻿using Abilities.AbilityPanel.Common.Components;
using Abilities.AbilityPanel.Common.Events;
using Leopotam.Ecs;
using Leopotam.Ecs.Ui.Components;

namespace Abilities.AbilityPanel.Weapon
{
    public class WeaponAbilityButtonSubsystem : IEcsRunSystem
    {
        private EcsFilter<WeaponAbilityButtonTag, AbilityButtonComponent> _weaponButtonEntity;
        private EcsFilter<EcsUiClickEvent> _clickEvent;

        public void Run()
        {
            foreach (var i in _clickEvent)
            {
                if (_clickEvent.Get1(i).WidgetName == "WeaponAbilityButton")
                {
                    _weaponButtonEntity.GetEntity(0).Get<AbilityButtonClickEvent>();
                }
            }
        }
    }
}