﻿using Abilities.RecoveryAbility.Events;
using Abilities.RecoveryAbility.Subsystems;
using Leopotam.Ecs;

namespace Abilities.RecoveryAbility.Systems
{
    public class RecoveryAbilitySystem : IEcsInitSystem, IEcsRunSystem, IEcsDestroySystem
    {
        private EcsWorld _world;
        private EcsSystems _subsystems;

        public void Init()
        {
            _subsystems = new EcsSystems(_world);
            _subsystems
                .Add(new HealthRecoveryAbilitySubsystem())
                .Add(new ArmorRecoveryAbilitySubsystem())
                .OneFrame<HealthRecoveryAbilityEvent>()
                .OneFrame<ArmorRecoveryAbilityEvent>()
                .Init();
        }

        public void Run()
        {
            _subsystems?.Run();
        }

        public void Destroy()
        {
            _subsystems?.Destroy();
        }
    }
}