﻿using ScriptsSO.Ability.Active.Recovery;

namespace Abilities.RecoveryAbility.Events
{
    public struct ArmorRecoveryAbilityEvent
    {
        public int Armor;
        public ArmorRecoveryAbility Ability;
    }
}