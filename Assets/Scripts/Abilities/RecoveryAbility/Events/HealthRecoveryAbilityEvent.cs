﻿using ScriptsSO.Ability.Active.Recovery;

namespace Abilities.RecoveryAbility.Events
{
    public struct HealthRecoveryAbilityEvent
    {
        public int Health;
        public HealthRecoveryAbility Ability;
    }
}