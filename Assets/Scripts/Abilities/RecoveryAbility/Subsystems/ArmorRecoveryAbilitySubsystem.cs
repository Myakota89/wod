﻿using Abilities.AbilityPanel.Common.Components;
using Abilities.RecoveryAbility.Events;
using Equipments.Head;
using Equipments.Platform;
using Equipments.Weapon;
using Input.Components;
using Leopotam.Ecs;
using Player;
using Popups;
using Popups.RecoveryArmorPopup.Providers;

namespace Abilities.RecoveryAbility.Subsystems
{
    public class ArmorRecoveryAbilitySubsystem : IEcsRunSystem
    {
        private EcsFilter<InputComponent, ArmorRecoveryAbilityEvent> _filter;
        private EcsFilter<PlayerTag, HeadComponent, PlatformComponent, WeaponComponent> _playerEntity;

        public void Run()
        {
            foreach (var i in _filter)
            {
                var recovery = _filter.Get2(i);
                var head = _playerEntity.Get2(0);
                var platform = _playerEntity.Get3(0);
                var weapon = _playerEntity.Get4(0);
                
                var recoveryHealthPopup = PopupManager.OpenPopup<RecoveryArmorPopup>();
                recoveryHealthPopup.SetArmorValue(recovery.Armor);
                recoveryHealthPopup.SetTitle($"Heal armor +{recovery.Armor}");
                recoveryHealthPopup.SetHeadData(head);
                recoveryHealthPopup.SetPlatformData(platform);
                recoveryHealthPopup.SetWeaponData(weapon);
                recoveryHealthPopup.SetWeaponData(weapon);
                
                recovery.Ability.Reload();
            }
        }
    }
}