﻿using ScriptsSO.Ability.Active.Heal;

namespace Abilities.HealEquipmentAbility.Events
{
    public struct HealAbilityEvent
    {
        public int HealthHeal;
        public int ArmorHeal;
        public HealAbility Ability;
    }
}