﻿using Abilities.AbilityPanel.Common.Components;
using Abilities.HealEquipmentAbility.Events;
using Heal.Events;
using Input.Components;
using Leopotam.Ecs;
using Player;

namespace Abilities.HealEquipmentAbility.Systems
{
    public class HealAbilitySystem : IEcsInitSystem, IEcsRunSystem, IEcsDestroySystem
    {
        private EcsFilter<InputComponent, HealAbilityEvent> _filter;
        private EcsFilter<PlayerTag> _playerEntity;

        private EcsWorld _world;
        private EcsSystems _subsystems;

        public void Init()
        {
            _subsystems = new EcsSystems(_world);
            _subsystems
                .OneFrame<HealAbilityEvent>()
                .Init();
        }

        public void Run()
        {
            foreach (var i in _filter)
            {
                ref var heal = ref _filter.Get2(i);
                ref var playerHeal = ref _playerEntity.GetEntity(0).Get<HealEvent>();

                playerHeal.ArmorHeal += heal.ArmorHeal;
                playerHeal.HealthHeal += heal.HealthHeal;
                
                heal.Ability.Reload();
            }
            
            _subsystems?.Run();
        }

        public void Destroy()
        {
            _subsystems?.Destroy();
        }
    }
}