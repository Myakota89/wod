﻿using Abilities.AbilityPanel.Common.Systems;
using Abilities.CellDamageAbility.Systems;
using Abilities.HealEquipmentAbility.Systems;
using Abilities.RecoveryAbility.Systems;
using Leopotam.Ecs;

namespace Abilities
{
    public class AbilitySystem : IEcsInitSystem, IEcsRunSystem, IEcsDestroySystem
    {
        private EcsWorld _world;
        private EcsSystems _subsystems;
        
        public void Init()
        {
            _subsystems = new EcsSystems(_world);
            _subsystems
                .Add(new AbilityPanelSystem())
                .Add(new HealAbilitySystem())
                .Add(new RecoveryAbilitySystem())
                .Add(new CellDamageAbilitySystem())
                .Init();
        }

        public void Run()
        {
            _subsystems?.Run();
        }

        public void Destroy()
        {
            _subsystems?.Destroy();
        }
    }
}