﻿using Abilities.CellDamageAbility.Events;
using Audio;
using Core;
using Damage.Components;
using Enum;
using Leopotam.Ecs;
using Units.Battle.Common;

namespace Abilities.CellDamageAbility.Systems
{
    public class CellDamageAbilitySystem : IEcsInitSystem, IEcsRunSystem, IEcsDestroySystem
    {
        private EcsFilter<BattleComponent, CellDamageAbilityEvent> _filter;
        
        private EcsWorld _world;
        private EcsSystems _subsystems;
        
        public void Init()
        {
            _subsystems = new EcsSystems(_world);
            _subsystems
                .OneFrame<CellDamageAbilityEvent>()
                .Init();
        }

        public void Run()
        {
            foreach (var i in _filter)
            {
                ref var ability = ref _filter.Get2(i);
                
                _filter.GetEntity(i).Get<DamageEvent>().Type = DamageType.DamageByAbility;
                _filter.GetEntity(i).Get<DamageEvent>().Value = ability.Damage;
                ability.ActiveAbility.Reload();
                AudioManager.PlayAudio(SoundType.CellDamage);
            }
            
            _subsystems?.Run();
        }

        public void Destroy()
        {
            _subsystems?.Destroy();
        }
    }
}