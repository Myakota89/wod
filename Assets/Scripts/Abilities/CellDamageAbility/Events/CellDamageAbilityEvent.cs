﻿using ScriptsSO.Ability.Active;
using UnityEngine;

namespace Abilities.CellDamageAbility.Events
{
    public struct CellDamageAbilityEvent
    {
        public int Damage;
        public ActiveAbility ActiveAbility;
    }
}