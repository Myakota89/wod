﻿using System;
using DG.Tweening;
using ScriptsSO.Ability.Active;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace ButtonView
{
    public enum ButtonState
    {
        Reload,
        Deselect,
        Select
    }
    
    public class AbilityButton : MonoBehaviour
    {
        [SerializeField] private Image icon;
        [SerializeField] private Image background;
        [SerializeField] private TextMeshProUGUI cooldownText;
        [SerializeField] private Transform leftDoor;
        [SerializeField] private Transform rightDoor;
        [SerializeField] private float doorSpeed;
        [SerializeField] private Ease doorEase;
        
        public ActiveAbility Ability { get; private set; }

        private Sequence _sequence;

        public void SetAbility(ActiveAbility ability)
        {
            if (Ability == ability)
                return;

            if (Ability == null)
            {
                icon.sprite = ability.AbilitySprite;
                Show();
            }
            else
            {
                Hide(() =>
                {
                    icon.sprite = ability.AbilitySprite;
                    Show();
                });
            }

            Ability = ability;
        }

        public void SetButtonState(ButtonState state)
        {
            switch (state)
            {
                case ButtonState.Reload:
                    background.color = Color.red;
                    break;
                case ButtonState.Deselect:
                    background.color = Color.white;
                    break;
                case ButtonState.Select:
                    background.color = Color.green;
                    break;
            }
        }

        public void SetCooldown(int value)
        {
            cooldownText.gameObject.SetActive(value > 0);
            cooldownText.text = value.ToString();
        }

        private void Show()
        {
            _sequence.Kill();
            _sequence = DOTween.Sequence();
            
            _sequence.Join(leftDoor.transform.DOLocalMoveX(-56f, doorSpeed))
                .Join(rightDoor.transform.DOLocalMoveX(56f, doorSpeed))
                .SetEase(doorEase);
        }

        private void Hide(Action callback = null)
        {
            _sequence.Kill();
            _sequence = DOTween.Sequence();
            
            _sequence
                .Join(leftDoor.transform.DOLocalMoveX(-17.6f, doorSpeed))
                .Join(rightDoor.transform.DOLocalMoveX(17.6f, doorSpeed))
                .SetEase(doorEase)
                .OnComplete(() => callback?.Invoke());
        }
    }
}