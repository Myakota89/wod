﻿using Abilities.AbilityPanel.Common.Components;
using Common.Components;
using Enum;
using GameCamera.Components;
using Input.Components;
using Input.Subsystems;
using Leopotam.Ecs;
using State;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Input.Systems
{
    public class InputSystem : IEcsInitSystem, IEcsRunSystem, IEcsDestroySystem
    {
        private EcsFilter<ColliderComponent> _filter;
        private EcsFilter<CameraComponent> _cameraEntity;
        private EcsFilter<InputComponent> _inputEntity;

        private EcsWorld _world;
        private EcsSystems _subsystems;

        public void Init()
        {
            _subsystems = new EcsSystems(_world);
            _subsystems
                .Add(new BattleUnitTapSubsystem())
                .Add(new CollectUnitTapSubsystem())
                .Add(new CellTapSubsystem())
                .Init();
        }

        public void Run()
        {
            if (!_inputEntity.Get1(0).PlayerControl)
                return;

            if (UnityEngine.Input.GetMouseButtonDown(0))
            {
                Ray ray = _cameraEntity.Get1(0).Camera.ScreenPointToRay(UnityEngine.Input.mousePosition);
                RaycastHit2D hit = Physics2D.Raycast(ray.origin, Vector2.zero);
                
                foreach (var i in _filter)
                {
                    if (hit.collider == _filter.Get1(i).Collider && !EventSystem.current.IsPointerOverGameObject())
                    {
                        _inputEntity.Get1(0).PlayerControl = false;
                        
                        if (_inputEntity.Get1(0).SelectedAbility != null)
                        {
                            _inputEntity.Get1(0).SelectedAbility.SetComponent( _filter.GetEntity(i));
                            _inputEntity.Get1(0).SelectedAbility = null;
                        }
                        else
                            _filter.GetEntity(i).Get<SimpleTapEvent>();
                    }
                }
            }
            
            _subsystems?.Run();
        }

        public void Destroy()
        {
            _subsystems?.Destroy();
        }
    }
}