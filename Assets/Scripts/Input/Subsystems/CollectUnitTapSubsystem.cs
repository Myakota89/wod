﻿using System;
using CellsMove.Events;
using Common.Components;
using Destroy.Components;
using Input.Components;
using Leopotam.Ecs;
using Player;
using Units.Collect.Common;

namespace Input.Subsystems
{
    public class CollectUnitTapSubsystem : IEcsRunSystem
    {
        private EcsFilter<UnitComponent, CollectComponent, SimpleTapEvent> _filter;
        private EcsFilter<PlayerTag, UnitComponent> _playerEntity;

        private EcsWorld _world;

        public void Run()
        {
            foreach (var i in _filter)
            {
                if (IsNeighbour(_filter.Get1(i)))
                {
                    ref var playerUnit = ref _playerEntity.Get2(0);
                    ref var unit = ref _filter.Get1(i);

                    var entity = _world.NewEntity();
                    entity.Get<CellsMoveEvent>().StartPosition = playerUnit.Position;
                    entity.Get<CellsMoveEvent>().TargetPosition = unit.Position;
                
                    _filter.GetEntity(i).Get<DestroyEvent>();
                }
            }
        }
        
        private bool IsNeighbour(UnitComponent unitComponent)
        {
            ref var playerUnitData = ref _playerEntity.Get2(0);
            var differenceX = Math.Abs(unitComponent.Position.x - playerUnitData.Position.x);
            var differenceY = Math.Abs(unitComponent.Position.y - playerUnitData.Position.y);

            return differenceX == 0 && differenceY == 1 ||
                   differenceX == 1 && differenceY == 0;

        }
    }
}