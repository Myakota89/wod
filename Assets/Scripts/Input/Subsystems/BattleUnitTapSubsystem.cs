﻿using System;
using Animation.Damage.Components;
using Common.Components;
using Damage.Components;
using Enum;
using Input.Components;
using Leopotam.Ecs;
using Player;
using Units.Battle.Common;

namespace Input.Subsystems
{
    public class BattleUnitTapSubsystem : IEcsRunSystem
    {
        private EcsFilter<UnitComponent, BattleComponent, SimpleTapEvent>.Exclude<PlayerTag> _filter;
        private EcsFilter<PlayerTag, UnitComponent, BattleComponent> _playerEntity;

        public void Run()
        {
            foreach (var i in _filter)
            {
                if (IsNeighbour(_filter.Get1(i)))
                {
                    ref var damageEvent = ref _filter.GetEntity(i).Get<DamageAnimationEvent>();
                    damageEvent.Callback = () =>
                    {
                        ref var damage = ref _filter.GetEntity(i).Get<DamageEvent>();
                        damage.Value = _playerEntity.Get3(0).Armor + _playerEntity.Get3(0).Health;
                        damage.Type = DamageType.DamageByPlayer;

                        ref var playerDamage = ref _playerEntity.GetEntity(0).Get<DamageEvent>();
                        playerDamage.Value = _filter.Get2(i).Armor + _filter.Get2(i).Health;
                        playerDamage.Type = DamageType.DamageByEnemy;
                    };
                }
            }
        }
        
        private bool IsNeighbour(UnitComponent unitComponent)
        {
            ref var playerUnitData = ref _playerEntity.Get2(0);
            var differenceX = Math.Abs(unitComponent.Position.x - playerUnitData.Position.x);
            var differenceY = Math.Abs(unitComponent.Position.y - playerUnitData.Position.y);

            return differenceX == 0 && differenceY == 1 ||
                   differenceX == 1 && differenceY == 0;

        }
    }
}