﻿using System;
using CellsMove.Events;
using Common.Components;
using Input.Components;
using Leopotam.Ecs;
using Move.Components;
using Player;
using Units.Battle.Cell.Components;

namespace Input.Subsystems
{
    public class CellTapSubsystem : IEcsRunSystem
    {
        private EcsFilter<CellTag, UnitComponent, SimpleTapEvent> _filter;
        private EcsFilter<PlayerTag, UnitComponent> _playerEntity;

        private EcsWorld _world;

        public void Run()
        {
            foreach (var i in _filter)
            {
                if (IsNeighbour(_filter.Get2(i)))
                {
                    //_playerEntity.GetEntity(0).Get<MovableEvent>().Speed = 1;
                    //_playerEntity.GetEntity(0).Get<MovableEvent>().DestinationPosition = _filter.Get2(i).Position;

                    var entity = _world.NewEntity();
                    entity.Get<CellsMoveEvent>().StartPosition = _playerEntity.Get2(0).Position;
                    entity.Get<CellsMoveEvent>().TargetPosition = _filter.Get2(i).Position;
                }
            }
        }
        
        private bool IsNeighbour(UnitComponent unitComponent)
        {
            ref var playerUnitData = ref _playerEntity.Get2(0);
            var differenceX = Math.Abs(unitComponent.Position.x - playerUnitData.Position.x);
            var differenceY = Math.Abs(unitComponent.Position.y - playerUnitData.Position.y);

            return differenceX == 0 && differenceY == 1 ||
                   differenceX == 1 && differenceY == 0;

        }
    }
}