﻿using System;
using ScriptsSO.Ability.Active;

namespace Input.Components
{
    [Serializable]
    public struct InputComponent
    {
        public bool PlayerControl;
        public ActiveAbility SelectedAbility;
    }
}