﻿namespace Enum
{
    public enum BonusType
    {
        None,
        ArmorBonusRefill,
        HealthBonusRefill,
        EquipmentBonus
    }
}