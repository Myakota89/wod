﻿namespace Enum
{
    public enum PopupType
    {
        PausePopup,
        GameOverPopup,
        WinPopup,
        EquipmentPopup
    }
}