﻿namespace Enum
{
    public enum GameState
    {
        Play,
        Win,
        Lose,
        Pause
    }
}