﻿namespace Enum
{
    public enum UnitState
    {
        Idle,
        Attack,
        Moving,
        Dead,
        Destroy,
        Animated,
        Destroyed
    }
}