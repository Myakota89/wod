﻿namespace Enum
{
    public enum TurnState
    {
        AwaitInput,
        PlayerMove,
        CameraMoving
    }
}