﻿namespace Enum
{
    public enum BoardUnitType
    {
        Player,
        Enemy,
        Item,
        Cell,
        Boss
    }
}