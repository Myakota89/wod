﻿namespace Enum
{
    public enum DamageType
    {
        DamageByPlayer,
        DamageByEnemy,
        DamageByAbility
    }
}