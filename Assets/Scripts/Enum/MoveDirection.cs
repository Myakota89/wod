﻿namespace Enum
{
    public enum MoveDirection
    {
        None,
        Up,
        Right,
        Down,
        Left
    }
}