﻿using Voody.UniLeo;

namespace Equipments.Head
{
    public class HeadProvider : MonoProvider<HeadComponent>
    {
        public HeadComponent Value
        {
            get => value;
            set => this.value = value;
        }
    }
}