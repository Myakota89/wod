﻿using Damage.Components;
using Equipments.Common;
using Leopotam.Ecs;
using Units.Battle.Common;

namespace Equipments.Head
{
    public class HeadSystem : IEcsRunSystem
    {
        private EcsFilter<BattleComponent, HeadComponent, EquipmentViewComponent> filter;

        public void Run()
        {
            foreach (var i in filter)
            {
                ref var battleComponent = ref filter.Get1(i);
                ref var headComponent = ref filter.Get2(i);
                ref var viewComponent = ref filter.Get3(i);
                
                battleComponent.ArmorSetValue += headComponent.CurrentArmor;
                battleComponent.HealthSetValue += headComponent.CurrentHealth;

                viewComponent.Head.gameObject.SetActive(headComponent.IsAlive);
                viewComponent.Head.sprite = headComponent.Sprite;
            }
        }
    }
}