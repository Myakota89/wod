﻿using System;
using Enum;
using ScriptsSO.Ability.Active;
using UnityEngine;

namespace Equipments.Head
{
    [Serializable]
    public struct HeadComponent
    {
        public int MaxHealth;
        public int CurrentHealth;
        public int MaxArmor;
        public int CurrentArmor;
        public bool IsAlive => CurrentHealth + CurrentArmor > 0;

        public HeadType HeadType;
        public ActiveAbility ActiveAbility;
        
        public Sprite Sprite;
    }
}