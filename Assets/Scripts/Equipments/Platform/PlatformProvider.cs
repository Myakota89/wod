﻿using Voody.UniLeo;

namespace Equipments.Platform
{
    public class PlatformProvider : MonoProvider<PlatformComponent>
    {
        public PlatformComponent Value
        {
            get => value;
            set => this.value = value;
        }
    }
}