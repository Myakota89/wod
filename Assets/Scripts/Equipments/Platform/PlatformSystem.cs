﻿using Damage.Components;
using Equipments.Common;
using Leopotam.Ecs;
using Units.Battle.Common;

namespace Equipments.Platform
{
    public class PlatformSystem : IEcsRunSystem
    {
        private EcsFilter<BattleComponent, PlatformComponent, EquipmentViewComponent> filter;

        public void Run()
        {
            foreach (var i in filter)
            {
                ref var battleComponent = ref filter.Get1(i);
                ref var platformComponent = ref filter.Get2(i);
                ref var viewComponent = ref filter.Get3(i);
                
                battleComponent.ArmorSetValue += platformComponent.CurrentArmor;
                battleComponent.HealthSetValue += platformComponent.CurrentHealth;

                viewComponent.Platform.gameObject.SetActive(platformComponent.IsAlive);
                viewComponent.Platform.sprite = platformComponent.Sprite;
            }
        }
    }
}