﻿using System;
using Enum;
using ScriptsSO.Ability.Active;
using UnityEngine;

namespace Equipments.Platform
{
    [Serializable]
    public struct PlatformComponent
    {
        public int MaxHealth;
        public int CurrentHealth;
        public int MaxArmor;
        public int CurrentArmor;
        public bool IsAlive => CurrentHealth + CurrentArmor > 0;

        public PlatformType PlatformType;
        public ActiveAbility ActiveAbility;
        
        public Sprite Sprite;
    }
}