﻿using Damage.Components;
using Equipments.Common;
using Equipments.Head;
using Equipments.Platform;
using Equipments.Weapon;
using Leopotam.Ecs;
using Units.Battle.Common;

namespace Equipments.Systems
{
    public class EquipmentSystem : IEcsInitSystem, IEcsRunSystem, IEcsDestroySystem
    {
        private EcsFilter<BattleComponent, StatsViewComponent> filter;

        private EcsWorld _world;
        private EcsSystems equipmentSystems;
        
        public void Init()
        {
            equipmentSystems = new EcsSystems(_world);
            equipmentSystems
                .Add(new WeaponSystem())
                .Add(new HeadSystem())
                .Add(new PlatformSystem())
                .Init();
        }
        
        public void Run()
        {
            equipmentSystems?.Run();
            
            foreach (var i in filter)
            {
                ref var battleComponent = ref filter.Get1(i);
                ref var view = ref filter.Get2(i);

                battleComponent.Armor = battleComponent.ArmorSetValue;
                battleComponent.Health = battleComponent.HealthSetValue;
                
                view.ArmorText.text = battleComponent.Armor.ToString();
                view.HealthText.text = battleComponent.Health.ToString();

                view.ArmorSprite.gameObject.SetActive(battleComponent.Armor > 0);
                view.HealthSprite.gameObject.SetActive(battleComponent.Health > 0);
                
                battleComponent.ArmorSetValue = 0;
                battleComponent.HealthSetValue = 0;
            }
        }

        public void Destroy()
        {
            equipmentSystems?.Destroy();
        }
    }
}