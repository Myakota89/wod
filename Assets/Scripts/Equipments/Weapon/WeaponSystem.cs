﻿using Damage.Components;
using Equipments.Common;
using Leopotam.Ecs;
using Units.Battle.Common;

namespace Equipments.Weapon
{
    public class WeaponSystem : IEcsRunSystem
    {
        private EcsFilter<BattleComponent, WeaponComponent, EquipmentViewComponent> weaponFilter;

        public void Run()
        {
            foreach (var i in weaponFilter)
            {
                ref var battleComponent = ref weaponFilter.Get1(i);
                ref var weaponComponent = ref weaponFilter.Get2(i);
                ref var viewComponent = ref weaponFilter.Get3(i);

                battleComponent.ArmorSetValue += weaponComponent.CurrentArmor;
                battleComponent.HealthSetValue += weaponComponent.CurrentHealth;

                viewComponent.LeftWeapon.gameObject.SetActive(weaponComponent.IsAlive);
                viewComponent.RightWeapon.gameObject.SetActive(weaponComponent.IsAlive);

                viewComponent.LeftWeapon.sprite = weaponComponent.LeftWeaponSprite;
                viewComponent.RightWeapon.sprite = weaponComponent.RightWeaponSprite;
            }
        }
    }
}