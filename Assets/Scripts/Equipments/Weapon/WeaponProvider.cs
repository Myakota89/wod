﻿using Voody.UniLeo;

namespace Equipments.Weapon
{
    public class WeaponProvider : MonoProvider<WeaponComponent>
    {
        public WeaponComponent Value
        {
            get => value;
            set => this.value = value;
        }
    }
}