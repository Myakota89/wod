﻿using System;
using TMPro;
using UnityEngine;

namespace Equipments.Common
{
    [Serializable]
    public struct StatsViewComponent
    {
        public SpriteRenderer ArmorSprite;
        public SpriteRenderer HealthSprite;

        public TextMeshPro ArmorText;
        public TextMeshPro HealthText;
    }
}