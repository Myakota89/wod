﻿using System;
using UnityEngine;

namespace Equipments.Common
{
    [Serializable]
    public struct EquipmentViewComponent
    {
        public SpriteRenderer Head;
        public SpriteRenderer Platform;
        public SpriteRenderer LeftWeapon;
        public SpriteRenderer RightWeapon;
    }
}