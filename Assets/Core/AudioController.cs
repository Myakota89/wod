﻿using UnityEngine;
using System;
using System.Collections.Generic;
using System.Linq;
using Core;
using DG.Tweening;

public class AudioController : MonoBehaviour
{
    [SerializeField] private AudioSource musicSource;
    [SerializeField] private List<AudioSource> soundSources;
    [SerializeField] private List<AudioData> AudioDatas;
    [SerializeField] private float volumeDownDuration = 3;
    [SerializeField] private float volumeUpDuration = 3;
    [SerializeField] private float soundPower;
    [SerializeField] private float musicPower;
    
    private Tween _musicVolumeTween;
    private Sequence _musicSequence;
    private Sequence _soundSequence;

    private void Start()
    {
        musicSource.volume = musicPower;
    }

    private void OnMusicChanged(bool on)
    {
        musicSource.mute = !on;
    }

    public void PlayAudioClip(SoundType soundType, bool loop = false, float volumeRatio = 1)
    {
        if (soundType == SoundType.None)
            return;

        var soundClip = AudioDatas.FirstOrDefault(s => s.SoundType == soundType);

       if (soundClip == null)
           throw new UnityException("Sounds not contains " + soundType);

        if (volumeRatio > 1)
            volumeRatio = 1;
        else if (volumeRatio < 0)
            volumeRatio = 0;

        var freeSoundSource = soundSources.FirstOrDefault(audio => !audio.isPlaying);
        if (freeSoundSource == default)
        {
            Debug.LogError("All AudioSources is busy for " + soundType);
            return;
        }
        
        freeSoundSource.loop = loop;
        freeSoundSource.clip = soundClip.AudioClip;
        freeSoundSource.volume = soundPower * volumeRatio;
        freeSoundSource.Play();
    }
    
    public void StopPlayedSound(SoundType soundType, bool smoothly = false)
    {
        if (soundType == SoundType.None)
            return;

        var audioData = AudioDatas.FirstOrDefault(s => s.SoundType == soundType);

        if (audioData == null)
            throw new UnityException("Sounds not contains " + soundType);
        
        var playedSources = soundSources.Where(audio => audio.isPlaying && audio.clip == audioData.AudioClip);
        if (smoothly)
        {
            foreach (var playedSource in playedSources)
            {
                SmoothlyVolumeDown(playedSource, volumeDownDuration);
            }
        }
        else
        {
            foreach (var playedSource in playedSources)
            {
                playedSource.Stop();
            }
        }
    }

    /*public void PlayAudioClipFromMusicMap(MusicTrackType musicTrackType)
    {
        if (!musicMap.ContainsKey(musicTrackType))
            throw new UnityException("MusicMap not contains " + musicTrackType);

        if (musicSource.isPlaying)
        {
            if (musicSource.clip == musicMap[musicTrackType])
                return;
            
            SmoothlyVolumeDown(musicSource, volumeDownDuration, callback:() =>
            {
                musicSource.clip = musicMap[musicTrackType];
                musicSource.Play();
                SmoothlyVolumeUp(musicSource, volumeUpDuration, musicPower);
            });
        }
        else
        {
            musicSource.clip = musicMap[musicTrackType];
            musicSource.volume = 0;
            musicSource.Play();
            SmoothlyVolumeUp(musicSource, volumeUpDuration,musicPower);
        }
    }*/

    public void MusicSmoothlyVolumeDown(float volumeRation) =>
        SmoothlyVolumeDown(musicSource, volumeDownDuration, musicPower * volumeRation);
    
    public void MusicSmoothlyVolumeUp() =>
        SmoothlyVolumeUp(musicSource, volumeUpDuration, musicPower);

    private void SmoothlyVolumeDown(AudioSource source, float duration, float endValue = 0, Action callback = null)
    { 
        _musicSequence?.Kill();
        _musicSequence = DOTween.Sequence();
        _musicSequence.Append(source.DOFade(endValue, duration)).SetLink(gameObject).onComplete = () => callback?.Invoke();
    }

    private void SmoothlyVolumeUp(AudioSource source, float duration, float endValue = 1)
    { 
        _musicSequence?.Kill();
        _musicSequence = DOTween.Sequence();
        _musicSequence.Append(source.DOFade(endValue, duration)).SetLink(gameObject);
    }
    
    public void PauseMusic()
    {
        if (musicSource.isPlaying)
            musicSource.Pause();
    }
    
    public void ResumeMusic()
    {
        if (!musicSource.isPlaying)
        {
            musicSource.UnPause();
        }
    }

    public void StopMusic()
    {
        if (musicSource.isPlaying)
            SmoothlyVolumeDown(musicSource, volumeDownDuration, callback:musicSource.Stop);
    }

}