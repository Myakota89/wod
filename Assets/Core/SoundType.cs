﻿namespace Core
{
    public enum SoundType
    {
        None,
        ClickButton,
        Repair,
        ArmorRefill,
        HealthRefill,
        CellDamage,
        MultiplyCellDamage,
        LineCellDamage,
        ImmortalBonus,
        FlyCell,
        UIOpenBonus,
        PlayerMoveCell,
        BossMoveCell,
        Win,
        Lose
        
    }
}