﻿using System;
using UnityEngine;

namespace Core
{
    [Serializable]
    public class AudioData
    {
        public SoundType SoundType;
        public AudioClip AudioClip;
    }
}